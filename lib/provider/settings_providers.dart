
import 'package:flutter/material.dart';
import 'package:muslam/utils/consts.dart';

class SettingsProvider with ChangeNotifier{
  Color _color = maleColor;
  String _gender = 'Male';
  double _fontSize = 18;

  Color get color => _color;
  String get gender => _gender;
  double get fontSize => _fontSize;

  void setGender(String gender) {
    _gender = gender;
    notifyListeners();
  }

  void setColor(Color color) {
    _color = color;
    notifyListeners();
  }

  void setFontSize(double size) {
    _fontSize = size;
    notifyListeners();
  }
}