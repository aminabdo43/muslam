import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:muslam/views/ui/reading/reading_page.dart';

class PageProvider with ChangeNotifier {
  int _currentPageIndex = 0;
  Widget _page = ReadingPage();

  void setPage(int pageIndex, Widget page, {int backIndex, Widget backPage}) {
    _currentPageIndex = backIndex ?? pageIndex;
    _page = backPage ?? page;
    notifyListeners();
  }

  int get pageIndex => _currentPageIndex;

  Widget get page => _page;
}
