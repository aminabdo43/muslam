class QuraanPageModel {
  dynamic _code;
  dynamic _status;
  dynamic _data;

  QuraanPageModel({dynamic code, dynamic status, dynamic data}) {
    this._code = code;
    this._status = status;
    this._data = data;
  }

  dynamic get code => _code;
  set code(int code) => _code = code;
  String get status => _status;
  set status(String status) => _status = status;
  Data get data => _data;
  set data(Data data) => _data = data;

  QuraanPageModel.fromJson(Map<String, dynamic> json) {
    _code = json['code'];
    _status = json['status'];
    _data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this._code;
    data['status'] = this._status;
    if (this._data != null) {
      data['data'] = this._data.toJson();
    }
    return data;
  }

  @override
  String toString() {
    return '$_data';
  }
}

class Data {
  dynamic _number;
  dynamic _ayahs;
  dynamic _surahs;
  dynamic _edition;

  @override
  String toString() {
    return '$_ayahs';
  }

  Data({int number, List<Ayahs> ayahs, Surahs surahs, Edition edition}) {
    this._number = number;
    this._ayahs = ayahs;
    this._surahs = surahs;
    this._edition = edition;
  }

  int get number => _number;
  set number(int number) => _number = number;
  List<Ayahs> get ayahs => _ayahs;
  set ayahs(List<Ayahs> ayahs) => _ayahs = ayahs;
  Surahs get surahs => _surahs;
  set surahs(Surahs surahs) => _surahs = surahs;
  Edition get edition => _edition;
  set edition(Edition edition) => _edition = edition;

  Data.fromJson(Map<String, dynamic> json) {
    _number = json['number'];
    if (json['ayahs'] != null) {
      _ayahs = new List<Ayahs>();
      json['ayahs'].forEach((v) {
        _ayahs.add(new Ayahs.fromJson(v));
      });
    }
    _surahs =
        json['surahs'] != null ? new Surahs.fromJson(json['surahs']) : null;
    _edition =
        json['edition'] != null ? new Edition.fromJson(json['edition']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this._number;
    if (this._ayahs != null) {
      data['ayahs'] = this._ayahs.map((v) => v.toJson()).toList();
    }
    if (this._surahs != null) {
      data['surahs'] = this._surahs.toJson();
    }
    if (this._edition != null) {
      data['edition'] = this._edition.toJson();
    }
    return data;
  }
}

class Ayahs {
  dynamic _number;
  dynamic _text;
  dynamic _surah;
  dynamic _numberInSurah;
  dynamic _juz;
  dynamic _manzil;
  dynamic _page;
  dynamic _ruku;
  dynamic _hizbQuarter;
  dynamic _sajda;

  Ayahs(
      {int number,
      String text,
      Surah surah,
      int numberInSurah,
      int juz,
      int manzil,
      int page,
      int ruku,
      int hizbQuarter,
      bool sajda}) {
    this._number = number;
    this._text = text;
    this._surah = surah;
    this._numberInSurah = numberInSurah;
    this._juz = juz;
    this._manzil = manzil;
    this._page = page;
    this._ruku = ruku;
    this._hizbQuarter = hizbQuarter;
    this._sajda = sajda;
  }

  int get number => _number;
  set number(int number) => _number = number;
  String get text => _text;
  set text(String text) => _text = text;
  Surah get surah => _surah;
  set surah(Surah surah) => _surah = surah;
  int get numberInSurah => _numberInSurah;
  set numberInSurah(int numberInSurah) => _numberInSurah = numberInSurah;
  int get juz => _juz;
  set juz(int juz) => _juz = juz;
  int get manzil => _manzil;
  set manzil(int manzil) => _manzil = manzil;
  int get page => _page;
  set page(int page) => _page = page;
  int get ruku => _ruku;
  set ruku(int ruku) => _ruku = ruku;
  int get hizbQuarter => _hizbQuarter;
  set hizbQuarter(int hizbQuarter) => _hizbQuarter = hizbQuarter;
  bool get sajda => _sajda;
  set sajda(bool sajda) => _sajda = sajda;

  Ayahs.fromJson(Map<String, dynamic> json) {
    _number = json['number'];
    _text = json['text'];
    _surah = json['surah'] != null ? new Surah.fromJson(json['surah']) : null;
    _numberInSurah = json['numberInSurah'];
    _juz = json['juz'];
    _manzil = json['manzil'];
    _page = json['page'];
    _ruku = json['ruku'];
    _hizbQuarter = json['hizbQuarter'];
    _sajda = json['sajda'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this._number;
    data['text'] = this._text;
    if (this._surah != null) {
      data['surah'] = this._surah.toJson();
    }
    data['numberInSurah'] = this._numberInSurah;
    data['juz'] = this._juz;
    data['manzil'] = this._manzil;
    data['page'] = this._page;
    data['ruku'] = this._ruku;
    data['hizbQuarter'] = this._hizbQuarter;
    data['sajda'] = this._sajda;
    return data;
  }

  @override
  String toString() {
    return '$_text ($numberInSurah)';
  }
}

class Surah {
  int _number;
  String _name;
  String _englishName;
  String _englishNameTranslation;
  String _revelationType;
  int _numberOfAyahs;

  Surah(
      {int number,
      String name,
      String englishName,
      String englishNameTranslation,
      String revelationType,
      int numberOfAyahs}) {
    this._number = number;
    this._name = name;
    this._englishName = englishName;
    this._englishNameTranslation = englishNameTranslation;
    this._revelationType = revelationType;
    this._numberOfAyahs = numberOfAyahs;
  }

  int get number => _number;
  set number(int number) => _number = number;
  String get name => _name;
  set name(String name) => _name = name;
  String get englishName => _englishName;
  set englishName(String englishName) => _englishName = englishName;
  String get englishNameTranslation => _englishNameTranslation;
  set englishNameTranslation(String englishNameTranslation) =>
      _englishNameTranslation = englishNameTranslation;
  String get revelationType => _revelationType;
  set revelationType(String revelationType) => _revelationType = revelationType;
  int get numberOfAyahs => _numberOfAyahs;
  set numberOfAyahs(int numberOfAyahs) => _numberOfAyahs = numberOfAyahs;

  Surah.fromJson(Map<String, dynamic> json) {
    _number = json['number'];
    _name = json['name'];
    _englishName = json['englishName'];
    _englishNameTranslation = json['englishNameTranslation'];
    _revelationType = json['revelationType'];
    _numberOfAyahs = json['numberOfAyahs'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['number'] = this._number;
    data['name'] = this._name;
    data['englishName'] = this._englishName;
    data['englishNameTranslation'] = this._englishNameTranslation;
    data['revelationType'] = this._revelationType;
    data['numberOfAyahs'] = this._numberOfAyahs;
    return data;
  }
}

class Surahs {
  Surah _s2;

  Surahs({Surah s2}) {
    this._s2 = s2;
  }

  Surah get s2 => _s2;
  set s2(Surah s2) => _s2 = s2;

  Surahs.fromJson(Map<String, dynamic> json) {
    _s2 = json['2'] != null ? new Surah.fromJson(json['2']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._s2 != null) {
      data['2'] = this._s2.toJson();
    }
    return data;
  }
}

class Edition {
  String _identifier;
  String _language;
  String _name;
  String _englishName;
  String _format;
  String _type;
  String _direction;

  Edition(
      {String identifier,
      String language,
      String name,
      String englishName,
      String format,
      String type,
      String direction}) {
    this._identifier = identifier;
    this._language = language;
    this._name = name;
    this._englishName = englishName;
    this._format = format;
    this._type = type;
    this._direction = direction;
  }

  String get identifier => _identifier;
  set identifier(String identifier) => _identifier = identifier;
  String get language => _language;
  set language(String language) => _language = language;
  String get name => _name;
  set name(String name) => _name = name;
  String get englishName => _englishName;
  set englishName(String englishName) => _englishName = englishName;
  String get format => _format;
  set format(String format) => _format = format;
  String get type => _type;
  set type(String type) => _type = type;
  String get direction => _direction;
  set direction(String direction) => _direction = direction;

  Edition.fromJson(Map<String, dynamic> json) {
    _identifier = json['identifier'];
    _language = json['language'];
    _name = json['name'];
    _englishName = json['englishName'];
    _format = json['format'];
    _type = json['type'];
    _direction = json['direction'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['identifier'] = this._identifier;
    data['language'] = this._language;
    data['name'] = this._name;
    data['englishName'] = this._englishName;
    data['format'] = this._format;
    data['type'] = this._type;
    data['direction'] = this._direction;
    return data;
  }
}
