
List<All> all = List<All>();

class All {
  dynamic soraName;
  dynamic seq;
  dynamic rakamAya;
  dynamic rakamSora;
  dynamic count2;
  dynamic aya;
  dynamic sala;

  All({
    this.soraName,
    this.seq,
    this.rakamAya,
    this.rakamSora,
    this.count2,
    this.aya,
    this.sala,
  });

  All.fromJson(Map<String, dynamic> json)
      : soraName = json['أسم السورة'],
        seq = json['Seq'],
        rakamAya = json['رقم الآية'],
        rakamSora = json['رقم السورة'],
        count2 = json['count2'],
        aya = json['aya'],
        sala = json['صلاة'];

  @override
  String toString() {
    return 'All{soraName: $soraName, seq: $seq, rakamAya: $rakamAya, rakamSora: $rakamSora, count2: $count2, aya: $aya, sala: $sala}';
  }
}
