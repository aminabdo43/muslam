List<PrayTime> prayTimeList = List<PrayTime>() ;

class PrayTime{
  dynamic date ;
  dynamic fagr ;
  dynamic sunrise ;
  dynamic dohr ;
  dynamic asr ;
  dynamic maghreb ;
  dynamic isha ;

  PrayTime(
      {this.date,
      this.fagr,
      this.sunrise,
      this.dohr,
      this.asr,
      this.maghreb,
      this.isha});

  PrayTime.fromJson(Map<String, dynamic> json)
      : date = json['date'],
        fagr = json['fagr'],
        sunrise = json['sunrise'],
        dohr = json['dohr'],
        asr = json['asr'],
        maghreb = json['maghreb'],
        isha = json['isha'];

  Map<String, dynamic> toJson() => {
        'date': date,
        'fagr': fagr,
        'sunrise': sunrise,
        'dohr': dohr,
        'asr': asr,
        'maghreb': maghreb,
        'isha': isha,
      };

  @override
  String toString() {
    return 'PrayTime{date: $date, fagr: $fagr, sunrise: $sunrise, dohr: $dohr, asr: $asr, maghreb: $maghreb, isha: $isha}';
  }

}