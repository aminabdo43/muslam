import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:provider/provider.dart';

Widget myButton2(
  BuildContext context,
  Widget child, {
  Function onTap,
  Color btnColor,
  BoxDecoration decoration,
  double width,
  TextStyle textStyle,
  double height,
  EdgeInsets margin,
}) {
  return Container(
    width: width ?? double.infinity,
    margin: margin,
    height: (height ?? 50),
    decoration: decoration ??
        BoxDecoration(
          color: btnColor ??
              Provider.of<SettingsProvider>(context, listen: false).color,
          borderRadius: BorderRadius.circular(5),
        ),
    child: FlatButton(
      padding: EdgeInsets.zero,
      onPressed: onTap,
      child: Center(child: child),
    ),
  );
}
