import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:share/share.dart';

String text = "القرآن الكريم هو جنة، هو رفعة ، هو هداية، هو سبيل إسعاد ودربُ أمان. إذا أردت أن تعلم ما عندك وعند غيرك من محبة الله فانظر محبة القرآن من قلبك";

void share(BuildContext context) {
  final RenderBox box = context.findRenderObject();
  Share.share(text, sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
}

Widget myAppBar(SettingsProvider settingsProvider, String text, BuildContext context, {Widget leading, Color bgColor, double elevation}) {
  return AppBar(
    elevation: elevation,
    backgroundColor: bgColor ?? settingsProvider.color,
    leading: leading ?? IconButton(
      icon: Icon(Icons.share),
      onPressed: () {
        share(context);
      },
    ),
    actions: [
      FlatButton(
        onPressed: null,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 8.0),
          child: Text(
            text,
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
      ),
    ],
  );
}
