import 'package:flutter/material.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

Widget progressDatePart(SettingsProvider settingsProvider,
    {bool useNormalDate ,bool landScape }) {
  bool _useNormalDate = useNormalDate ?? true;
  initializeDateFormatting();
  HijriCalendar hijriCalendar = HijriCalendar.setLocal('ar');
  var _today = HijriCalendar.now();
  // DateFormat("dd.MM.yyyy").format(DateTime.now());

  String meladyDate() {
    var now = new DateTime.now();
    var formatter = DateFormat.yMMMd("ar_SA");
    String formatted = formatter.format(now);
    print(formatted);
    return formatted;
  }

  print(calcProgress());
  Intl.defaultLocale = "zh_HK"; //sets global,
  //for specific Format instence only
  var newFormat = new DateFormat.yMMMMd('zh_HK');
  return Column(
    crossAxisAlignment: CrossAxisAlignment.end,
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Image.asset(
            'assets/images/mosque.png',
            width: 25,
            height: 25,
          ),
          Row(
            children: [
              Text(
                '${Pointer.currentPray == "الشروق" ? "الفجر" : Pointer.currentPray}',
                style: TextStyle(
                  fontSize: 16,
                  color: settingsProvider.color,
                ),
              ),
              SizedBox(
                width: 2,
              ),
              Text(
                'وقت صلاة',
                style: TextStyle(fontSize: 16, color: Colors.black),
              ),
            ],
          ),
          Text(
            'معدل التقدم',
            style: TextStyle(fontSize: 16, color: Colors.black),
          ),
        ],
      ),
      SizedBox(
        height: 8,
      ),
      RotatedBox(
        quarterTurns: 2,
        child: LinearPercentIndicator(
          lineHeight: 10.0,
          percent: calcProgress().clamp(0, 1),
          progressColor: settingsProvider.color,
        ),
      ),
      SizedBox(
        height: 14,
      ),
      Row(
        children: [
          _useNormalDate
              ? Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'التاريخ الهجري',
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        _today.fullDate(),
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                )
              : Container(),
          SizedBox(
            width: 10,
          ),
          _useNormalDate
              ? Container()
              : Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        'التاريخ الميلادي',
                        style: TextStyle(fontSize: 20),
                      ),
                      Text(
                        // DateTimeFormat.format(DateTime.now().toLocal(), format: DateTimeFormats.american),
                        meladyDate(),
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          fontSize: 13,
                        ),
                      ),
                    ],
                  ),
                ),
        ],
      ),
    ],
  );
}

double calcProgress() {
  if (Pointer.lastPart == null) {
    return 0.0;
  } else {
    print('>>>>>>>.. ${Pointer.lastPart.partIndex}');
    return (Pointer.lastPart.partIndex + 1) / 30;
  }
}
