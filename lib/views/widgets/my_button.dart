
import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:provider/provider.dart';

Widget myButton(
    BuildContext context,
    String title, {
      Function onTap,
      Color btnColor,
      BoxDecoration decoration,
      double width,
      TextStyle textStyle,
      double height,
      EdgeInsets margin,
    }) {
  return Container(
    width: width ?? double.infinity,
    margin: margin,
    height: (height ?? 50),
    decoration: decoration ??
        BoxDecoration(
          color: btnColor ?? Provider.of<SettingsProvider>(context, listen: false).color,
          borderRadius: BorderRadius.circular(5),
        ),
    child: ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: FlatButton(
        onPressed: onTap,
        padding: EdgeInsets.zero,
        child: Center(
          child: Text(
            title,
            textAlign: TextAlign.center,
            style: textStyle ??
                TextStyle(
                  color: Colors.white,
                  fontSize: Provider.of<SettingsProvider>(context, listen: false).fontSize,
                ),
          ),
        ),
      ),
    ),
  );
}