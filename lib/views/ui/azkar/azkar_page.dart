import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/consts.dart';

// ignore: must_be_immutable
class AzkarPage extends StatelessWidget {
  SettingsProvider settingsProvider = SettingsProvider();

  @override
  Widget build(BuildContext context) {
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Stack(
      children: [
       /* Image.asset(
          "assets/images/background.png",
          fit: BoxFit.cover,
          height: MediaQuery.of(context).size.height,
          width: landScape ? screenHeight*2 : screenWidth,
        ),*/
        Scaffold(
         // backgroundColor: Colors.transparent,
          backgroundColor: BACK_GROUND_COLOR,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(56),
            child: AppBar(
              // backgroundColor: settingsProvider.color,
              backgroundColor: settingsProvider.color,
              elevation: 0,
              centerTitle: true,
              title: Text("أذكار ما بعد الصلاة المفروضة"),
            ),
          ),
          body: Directionality(
            textDirection: TextDirection.rtl,
            child: FutureBuilder(
              future: DefaultAssetBundle.of(context)
                  .loadString("assets/azkar-data/post_prayer_azkar.json"),
              builder: (context, snapshot) {
                var showData = json.decode(snapshot.data.toString());
                if (showData == null) {
                  return SpinKitRipple(
                    color: Colors.white,
                    size: 70,
                  );
                } else {
                  return ListView.builder(
                    itemBuilder: (BuildContext context, int index) {
                      return Column(
                        children: [
                          ListTile(
                            title: Text(
                              showData['content'][index]['zekr'],
                              style: TextStyle(color: Colors.white),
                            ),
                            subtitle: Text(
                                "${showData['content'][index]['repeat']}",
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.6),
                                    fontWeight: FontWeight.w500)),
                            /*subtitle: Row(
                              children: [
                                Text(
                                  "عدد مرات التكرار : ",
                                  style: TextStyle(color: Colors.black),
                                ),
                                Text(
                                  showData['content'][index]['repeat'].toString(),
                                  style: TextStyle(color: Colors.black),
                                ),
                              ],
                            ),*/
                          ),
                          SizedBox(height: 15,)
                        ],
                      );
                    },
                    itemCount: showData['content'].length,
                  );
                }
              },
            ),
          ),
        )
      ],
    );
  }
}
