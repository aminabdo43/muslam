import 'package:flutter/material.dart';
import 'package:muslam/provider/network_provider.dart';
import 'package:muslam/provider/page_provider.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var networkProvider = Provider.of<NetworkProvider>(context);
    return Scaffold(
      body: networkProvider.hasNetworkConnection == null
          ? Center(child: CircularProgressIndicator())
          : !networkProvider.hasNetworkConnection
              ? Center(
                  child: Text('No Internet Connection'),
                )
              : Consumer<PageProvider>(
                  builder: (context, pageProvider, child) {
                    return pageProvider.page;
                  },
                ),
    );
  }
}
