import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:provider/provider.dart';

class DoaaElestftahPage extends StatefulWidget {
  @override
  _DoaaElestftahPageState createState() => _DoaaElestftahPageState();
}

class _DoaaElestftahPageState extends State<DoaaElestftahPage> {
  List<String> doaa = [
    'سبحانك اللهم وبحمدك وتبارك اسمك وتعالى جدك ولا إله غيرك',
    'اللهم باعد بيني وبين خطاياي كما باعدت بين المشرق والمغرب، اللهم نقني من خطاياي كما ينقى الثوب الأبيض من الدنس،، اللهم اغسلني من خطاياي بالثلج والماء والبرد.',
    'وَجَّهْتُ وَجْهِيَ لِلَّذِي فَطَرَ السَّمَاوَاتِ وَالْأَرْضَ حَنِيفًا، وَمَا أَنَا مِنَ الْمُشْرِكِينَ، إِنَّ صَلَاتِي، وَنُسُكِي، وَمَحْيَايَ، وَمَمَاتِي لِلَّهِ رَبِّ الْعَالَمِينَ، لَا شَرِيكَ لَهُ، وَبِذَلِكَ أُمِرْتُ وَأَنَا مِنَ الْمُسْلِمِينَ، اللهُمَّ أَنْتَ الْمَلِكُ لَا إِلَهَ إِلَّا أَنْتَ أَنْتَ رَبِّي، وَأَنَا عَبْدُكَ، ظَلَمْتُ نَفْسِي، وَاعْتَرَفْتُ بِذَنْبِي، فَاغْفِرْ لِي ذُنُوبِي جَمِيعًا، إِنَّهُ لَا يَغْفِرُ الذُّنُوبَ إِلَّا أَنْتَ، وَاهْدِنِي لِأَحْسَنِ الْأَخْلَاقِ لَا يَهْدِي لِأَحْسَنِهَا إِلَّا أَنْتَ، وَاصْرِفْ عَنِّي سَيِّئَهَا لَا يَصْرِفُ عَنِّي سَيِّئَهَا إِلَّا أَنْتَ، لَبَّيْكَ وَسَعْدَيْكَ وَالْخَيْرُ كُلُّهُ فِي يَدَيْكَ، وَالشَّرُّ لَيْسَ إِلَيْكَ، أَنَا بِكَ وَإِلَيْكَ، تَبَارَكْتَ وَتَعَالَيْتَ، أَسْتَغْفِرُكَ وَأَتُوبُ إِلَيْكَ.',
    'اللهُمَّ رَبَّ جَبْرَائِيلَ، وَمِيكَائِيلَ، وَإِسْرَافِيلَ، فَاطِرَ السَّمَاوَاتِ وَالْأَرْضِ، عَالِمَ الْغَيْبِ وَالشَّهَادَةِ، أَنْتَ تَحْكُمُ بَيْنَ عِبَادِكَ، فِيمَا كَانُوا فِيهِ يَخْتَلِفُونَ، اهْدِنِي لِمَا اخْتُلِفَ فِيهِ مِنَ الْحَقِّ بِإِذْنِكَ، إِنَّكَ تَهْدِي مَنْ تَشَاءُ إِلَى صِرَاطٍ مُسْتَقِيمٍ.',
    'اللَّهُمَّ لَكَ الحَمْدُ أَنْتَ نُورُ السَّمَوَاتِ وَالأَرْضِ، وَلَكَ الحَمْدُ أَنْتَ قَيِّمُ السَّمَوَاتِ وَالأَرْضِ، وَلَكَ الحَمْدُ أَنْتَ رَبُّ السَّمَوَاتِ وَالأَرْضِ وَمَنْ فِيهِنَّ، أَنْتَ الحَقُّ، وَوَعْدُكَ الحَقُّ، وَقَوْلُكَ الحَقُّ، وَلِقَاؤُكَ الحَقُّ، وَالجَنَّةُ حَقٌّ، وَالنَّارُ حَقٌّ، وَالنَّبِيُّونَ حَقٌّ، وَالسَّاعَةُ حَقٌّ، اللَّهُمَّ لَكَ أَسْلَمْتُ، وَبِكَ آمَنْتُ، وَعَلَيْكَ تَوَكَّلْتُ، وَإِلَيْكَ أَنَبْتُ، وَبِكَ خَاصَمْتُ، وَإِلَيْكَ حَاكَمْتُ، فَاغْفِرْ لِي مَا قَدَّمْتُ وَمَا أَخَّرْتُ، وَمَا أَسْرَرْتُ وَمَا أَعْلَنْتُ، أَنْتَ إِلَهِي لاَ إِلَهَ إِلَّا أَنْتَ.'
  ];

  String selectedDo3aa;
  String d = '';

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    return Scaffold(
      appBar: myAppBar(settingsProvider, 'دعاء الاستفتاح', context),
      body: SingleChildScrollView(
        child: Padding(
          padding: landScape
              ? EdgeInsets.fromLTRB(10,
              10, 10, 20)
              : const EdgeInsets.all(2.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              ListTile(
                onTap: () {
                  d = doaa[0];
                  setState(() {
                    selectedDo3aa = doaa[0];
                  });
                },
                trailing: Icon(
                  Icons.circle,
                  size: 14,
                  color: settingsProvider.color,
                ),
                title: Text(
                  'الدعاء الاول',
                  textAlign: TextAlign.right,
                  style: TextStyle(fontSize: settingsProvider.fontSize),
                ),
              ),
              ListTile(
                onTap: () {
                  d = doaa[1];
                  setState(() {
                    selectedDo3aa = doaa[1];
                  });
                },
                trailing: Icon(
                  Icons.circle,
                  size: 16,
                  color: settingsProvider.color,
                ),
                title: Text(
                  'الدعاء الثاني',
                  textAlign: TextAlign.right,
                  style: TextStyle(fontSize: settingsProvider.fontSize),
                ),
              ),
              ListTile(
                onTap: () {
                  d = doaa[2];
                  setState(() {
                    selectedDo3aa = doaa[2];
                  });
                },
                trailing: Icon(
                  Icons.circle,
                  size: 16,
                  color: settingsProvider.color,
                ),
                title: Text(
                  'الدعاء الثالث',
                  textAlign: TextAlign.right,
                  style: TextStyle(fontSize: settingsProvider.fontSize),
                ),
              ),
              ListTile(
                onTap: () {
                  d = doaa[3];
                  setState(() {
                    selectedDo3aa = doaa[3];
                  });
                },
                trailing: Icon(
                  Icons.circle,
                  size: 16,
                  color: settingsProvider.color,
                ),
                title: Text(
                  'الدعاء الرابع',
                  textAlign: TextAlign.right,
                  style: TextStyle(fontSize: settingsProvider.fontSize),
                ),
              ),
              ListTile(
                onTap: () {
                  d = doaa[4];
                  setState(() {
                    selectedDo3aa = doaa[4];
                  });
                },
                trailing: Icon(
                  Icons.circle,
                  size: 16,
                  color: settingsProvider.color,
                ),
                title: Text(
                  'الدعاء الخامس',
                  textAlign: TextAlign.right,
                  style: TextStyle(fontSize: settingsProvider.fontSize),
                ),
              ),
              Divider(),
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  d,
                  textAlign: TextAlign.right,
                  style: TextStyle(
                    fontSize: settingsProvider.fontSize,
                    fontFamily: 'ayat',
                  ),
                ),
              ),
              selectedDo3aa == null
                  ? Container()
                  : Center(
                      child: RaisedButton(
                          child: Text(
                            "اختيار هذا الدعاء",
                            style: TextStyle(color: Colors.white),
                          ),
                          color: settingsProvider.color,
                          onPressed: () {
                            setState(() {
                              AppUtils.setDo3aaAlestftah(selectedDo3aa);
                              print(selectedDo3aa);
                            });
                            final snackBar = SnackBar(
                              content: Padding(
                                padding: const EdgeInsets.only(right: 15),
                                child: Text(
                                  'تم',
                                  textDirection: TextDirection.rtl,
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              backgroundColor: settingsProvider.color,
                            );
                            ScaffoldMessenger.of(context).showSnackBar(snackBar);
                            setState(() {});
                          }),
                    )
            ],
          ),
        ),
      ),
    );
  }
}
