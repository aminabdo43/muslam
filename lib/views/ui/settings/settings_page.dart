import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/consts.dart';
import 'package:muslam/views/ui/azkar/azkar_page.dart';
import 'package:muslam/views/ui/settings/doaa_elestftah_page.dart';
import 'package:muslam/views/ui/settings/doaa_khetm_page.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:muslam/views/widgets/my_button.dart';
import 'package:muslam/views/widgets/progress_date_part.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  List<Color> colors = [
    maleColor,
    femaleColor,
    Color(0xff9BBB59),
    Color(0xffF79646),
  ];

  List<double> sizes = [
    18,
    20,
    22,
    24,
    26,
  ];

  int selectedColor = 0;
  int selectedSize = 0;
  bool getDefault = false;
  double sliderValue;

  SettingsProvider settingsProvider =SettingsProvider();
  @override
  void initState() {
    sliderValue = settingsProvider.fontSize.toDouble();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    if (!getDefault) {
      selectedColor = settingsProvider.color == colors[0]
          ? 0
          : settingsProvider.color == colors[1]
              ? 1
              : settingsProvider.color == colors[2]
                  ? 2
                  : 3;

      selectedSize = settingsProvider.fontSize == sizes[0]
          ? 0
          : settingsProvider.fontSize == sizes[1]
              ? 1
              : settingsProvider.fontSize == sizes[2]
                  ? 2
                  : settingsProvider.fontSize == sizes[3]
                      ? 3
                      : settingsProvider.fontSize == sizes[4]
                          ? 4
                          : 5;

      getDefault = true;
    }

    return Scaffold(
      appBar: myAppBar(settingsProvider, 'الاعدادات', context),
      body: Container(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Padding(
            padding: landScape
                ? EdgeInsets.fromLTRB(MediaQuery.of(context).size.width / 6,
                10, MediaQuery.of(context).size.width / 6, 20)
                : const EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                progressDatePart(settingsProvider),
                SizedBox(
                  height: MediaQuery.of(context).size.width / 14,
                ),
                Text(
                  'لون واجهة التطبيق',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: List.generate(
                    colors.length,
                    (index) => GestureDetector(
                      onTap: () {
                        selectedColor = index;
                        settingsProvider.setColor(colors[index]);
                        AppUtils.saveColor('$selectedColor');
                        setState(() {});
                      },
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: colors[index],
                        ),
                        child: selectedColor == index
                            ? Icon(
                                Icons.check,
                                color: Colors.white,
                                size: 20,
                              )
                            : SizedBox.shrink(),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width / 14,
                ),
                Text(
                  'حجم الخط',
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
                Directionality(child: buildSlider(),textDirection: TextDirection.rtl,),
                /*Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: List.generate(
                    sizes.length,
                    (index) => GestureDetector(
                      onTap: () {
                        selectedSize = index;
                        settingsProvider.setFontSize(sizes[selectedSize]);
                        AppUtils.saveFontSize(sizes[selectedSize]);
                        setState(() {});
                      },
                      child: AnimatedContainer(
                        duration: Duration(milliseconds: 500),
                        padding: EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: selectedSize == index
                              ? settingsProvider.color
                              : Colors.grey,
                        ),
                        child: Text(
                          '${sizes[index]}',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),*/
                SizedBox(
                  height: MediaQuery.of(context).size.width /14,
                ),
                myButton(
                  context,
                  'دعاء الاستفتاح',
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => DoaaElestftahPage(),),);
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width / 20,
                ),
                myButton(
                  context,
                  'اذكار بعد الصلاة',
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => AzkarPage(),),);
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width / 20,
                ),
                myButton(
                  context,
                  'دعاء ختم القران',
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (_) => DoaaKhetmPage(),),);
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildSlider() {
    return Slider(
        value: sliderValue,
        min: 14,
        max: 26,
        divisions: 6,
        label: sliderValue.toString(),
        onChanged: (value) {
          setState(() {
            sliderValue = value;
            Provider.of<SettingsProvider>(context, listen: false).setFontSize(value.toDouble());
          });
        });
  }
}
