import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/reading/reading_page.dart';
import 'package:muslam/views/ui/start/start_page.dart';
import 'package:provider/provider.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    super.initState();

    Future.delayed(
      Duration(seconds: 3),
      () {
        var settingsProvider =
            Provider.of<SettingsProvider>(context, listen: false);
        settingsProvider.setColor(Pointer.color);
        settingsProvider.setGender(Pointer.gender);
        settingsProvider.setFontSize(Pointer.fontSize);
        if (Pointer.isFirstTimeLaunch ?? true) {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (_) => StartPage(),
              ),
              (route) => false);
        } else {
          Navigator.of(context).pushAndRemoveUntil(
              MaterialPageRoute(
                builder: (_) => ReadingPage(),
              ),
              (route) => false);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Pointer.color,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "قرآني في صلاتي",
            style: TextStyle(
              fontSize: 50.0,
              color: Colors.white,
            ),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: 14,
          ),
          SpinKitRipple(
            color: Colors.white,
            size: 70,
          ),
        ],
      ),
    );
  }
}
