import 'package:adhan_flutter/adhan_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:location/location.dart';
import 'package:muslam/main.dart';
import 'package:muslam/provider/network_provider.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/home/home_page.dart';
import 'package:provider/provider.dart';

class LocationPage extends StatefulWidget {
  @override
  _LocationPageState createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  Adhan adhan;

  static const String LOCATION_TITLE = 'تفعيل الموقع';
  static const String LOCATION_DESCRIPTION =
      'قم بتفعيل خاصية الموقع لتسهيل الوصول للمواقيت الصحيحة للآذان';
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  NotificationAppLaunchDetails notificationAppLaunchDetails;

  LocationData _locationData;

  bool isLoading = false;

  @override
  Widget build(BuildContext context) {
    final mqw = MediaQuery.of(context).size.width;
    var networkProvider = Provider.of<NetworkProvider>(context);
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    var settingsProvider = Provider.of<SettingsProvider>(context);
    return Scaffold(
      body: networkProvider.hasNetworkConnection == null
          ? Center(child: CircularProgressIndicator())
          : !networkProvider.hasNetworkConnection
              ? Center(
                  child: Text('No Internet Connection'),
                )
              : Stack(
                  children: <Widget>[
                    Background(opacity: 0.2),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding:  EdgeInsets.only(top: landScape?20 :40),
                          child: Image.asset('assets/images/Location.png',
                            color: settingsProvider.color,
                            width: landScape
                                ? 50
                                : MediaQuery.of(context).size.width - 50,
                            height: landScape
                                ? 150
                                : MediaQuery.of(context).size.width - 150.0,
                          ),
                        ),
                        ScreenTitle(
                          title: LOCATION_TITLE,
                          color: settingsProvider.color,
                          landScape: landScape,
                        ),
                        CustomDescription(
                          text: LOCATION_DESCRIPTION,
                        ),
                        Column(
                          children: <Widget>[
                            ActiveButton(
                              width: landScape ? mqw / 2 : mqw - 50.0,
                              settingsProvider: settingsProvider,
                              height: 50.0,
                              text: LOCATION_TITLE,
                              onPressed: () => _activeButtonPressed(context),
                            ),
                            SizedBox(
                              height: landScape ? 0 : 18,
                            ),
                          ],
                        )
                      ],
                    ),
                    isLoading
                        ? Center(child: CircularProgressIndicator())
                        : Container()
                  ],
                ),
    );
  }

  void _activeButtonPressed(BuildContext context) async {
    setState(() {
      isLoading = true;
    });

    Location location = Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    print('>>>> start');
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      print('>>>> requestService');
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        print('>>>> !requestService');
        return;
      }
    }

    print('>>>> hasPermission');
    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      print('>>>> requestPermission');
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        print('>>>> !requestPermission');
        return;
      }
    }

    _locationData = await location.getLocation();

    AppUtils.saveUserLocationLatitude(_locationData.longitude);
    AppUtils.saveUserLocationLong(_locationData.longitude);

    Pointer.lat = _locationData.latitude;
    Pointer.long = _locationData.longitude;

    Prayer prayer =
        await getCurrentPrayer(_locationData.latitude, _locationData.longitude);
    print(getPrayerName(prayer));
    Pointer.currentPray = getPrayerName(prayer);

    // await initializeNotification();
    //
    // activateAzanNotification();

    setState(() {
      isLoading = false;
    });

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (_) => HomePage()),
      (route) => false,
    );
  }

  Future initializeNotification() async {
    notificationAppLaunchDetails =
        await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

    var initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    var initializationSettingsIOS = IOSInitializationSettings(
      requestAlertPermission: true,
      requestBadgePermission: true,
      requestSoundPermission: true,
      onDidReceiveLocalNotification:
          (int id, String title, String body, String payload) async {},
    );
    var initializationSettings = InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);

    await flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
      onSelectNotification: (String payload) async {},
    );
  }

  Future<void> activateAzanNotification() async {
    DateTime fajr = (await adhan.fajr);
    DateTime dhuhr = (await adhan.dhuhr);
    DateTime asr = (await adhan.asr);
    DateTime maghrib = (await adhan.maghrib);
    DateTime isa = (await adhan.isha);
    DateTime shrouk = (await adhan.sunrise);

    Map prayTimes = {
      NotifyKeys.FAJR: {
        NotifyKeys.HOUR: fajr.hour,
        NotifyKeys.MINUTE: fajr.minute,
      },
      NotifyKeys.DUHR: {
        NotifyKeys.HOUR: dhuhr.hour,
        NotifyKeys.MINUTE: dhuhr.minute,
      },
      NotifyKeys.ASR: {
        NotifyKeys.HOUR: asr.hour,
        NotifyKeys.MINUTE: asr.minute,
      },
      NotifyKeys.MEGHRIB: {
        NotifyKeys.HOUR: maghrib.hour,
        NotifyKeys.MINUTE: maghrib.minute,
      },
      NotifyKeys.ISHAA: {
        NotifyKeys.HOUR: isa.hour,
        NotifyKeys.MINUTE: isa.minute,
      },
      NotifyKeys.SHROUK: {
        NotifyKeys.HOUR: shrouk.hour,
        NotifyKeys.MINUTE: shrouk.minute,
      },
    };

    Map _times = {
      // get dates from adhan package
      NotifyKeys.FAJR: Time(prayTimes[NotifyKeys.FAJR][NotifyKeys.HOUR],
          prayTimes[NotifyKeys.FAJR][NotifyKeys.MINUTE], 0),
      NotifyKeys.DUHR: Time(prayTimes[NotifyKeys.DUHR][NotifyKeys.HOUR],
          prayTimes[NotifyKeys.DUHR][NotifyKeys.MINUTE], 0),
      NotifyKeys.ASR: Time(prayTimes[NotifyKeys.ASR][NotifyKeys.HOUR],
          prayTimes[NotifyKeys.ASR][NotifyKeys.MINUTE], 0),
      NotifyKeys.MEGHRIB: Time(prayTimes[NotifyKeys.MEGHRIB][NotifyKeys.HOUR],
          prayTimes[NotifyKeys.MEGHRIB][NotifyKeys.MINUTE], 0),
      NotifyKeys.ISHAA: Time(prayTimes[NotifyKeys.ISHAA][NotifyKeys.HOUR],
          prayTimes[NotifyKeys.ISHAA][NotifyKeys.MINUTE], 0),
      NotifyKeys.SHROUK: Time(prayTimes[NotifyKeys.SHROUK][NotifyKeys.HOUR],
          prayTimes[NotifyKeys.SHROUK][NotifyKeys.MINUTE], 0),
    };

    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      '${NotifyKeys.ISHAA_KEY} channel id',
      '${NotifyKeys.ISHAA_KEY} channel name',
      '${NotifyKeys.ISHAA_KEY} description',
      priority: Priority.Max,
      importance: Importance.Max,
    );

    var iOSPlatformChannelSpecifics = IOSNotificationDetails();

    var platformChannelSpecifics = NotificationDetails(
        androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    await flutterLocalNotificationsPlugin.showDailyAtTime(
      NotifyKeys.FAJR_KEY,
      'حان الآن موعد صلاة الفجر',
      null,
      _times[NotifyKeys.FAJR],
      platformChannelSpecifics,
    );

    await flutterLocalNotificationsPlugin.showDailyAtTime(
      NotifyKeys.SHROUK_KEY,
      'حان الآن موعد صلاة الشروق',
      null,
      _times[NotifyKeys.SHROUK],
      platformChannelSpecifics,
    );

    await flutterLocalNotificationsPlugin.showDailyAtTime(
        NotifyKeys.DUHR_KEY,
        'حان الآن موعد صلاة الظهر',
        null,
        _times[NotifyKeys.DUHR],
        platformChannelSpecifics);

    await flutterLocalNotificationsPlugin.showDailyAtTime(
        NotifyKeys.ASR_KEY,
        'حان الآن موعد صلاة العصر',
        null,
        _times[NotifyKeys.ASR],
        platformChannelSpecifics);

    await flutterLocalNotificationsPlugin.showDailyAtTime(
        NotifyKeys.MEGHRIB_KEY,
        'حان الآن موعد صلاة المغرب',
        null,
        _times[NotifyKeys.MEGHRIB],
        platformChannelSpecifics);

    await flutterLocalNotificationsPlugin.showDailyAtTime(
        NotifyKeys.ISHAA_KEY,
        'حان الآن موعد صلاة العشاء',
        null,
        _times[NotifyKeys.ISHAA],
        platformChannelSpecifics);

    await flutterLocalNotificationsPlugin.show(
      NotifyKeys.GENERAL_AZAN,
      'تم تفعيل إشعارات الآذان',
      null,
      platformChannelSpecifics,
    );
  }

  Future<Prayer> getCurrentPrayer(double lat, double long) async {
    print('>>>>>>>>>>. 1');
    adhan = AdhanFlutter.create(
      Coordinates(lat, long),
      DateTime.now(),
      CalculationMethod.EGYPTIAN,
    );

    print('>>>>>>>>>>. 2');

    return await adhan.currentPrayer();
  }
}

class Background extends StatelessWidget {
  final double opacity;

  Background({this.opacity = 0.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        decoration: BoxDecoration(color: Colors.black.withOpacity(opacity)),
      ),
    );
  }
}

class PaddedImage extends StatelessWidget {
  final double top, left, right, bottom, width, height, opacity;
  final String img;
  final bool landScape;

  PaddedImage(
      {@required this.width,
      @required this.height,
      this.top = 0.0,
      this.left = 0.0,
      this.right = 0.0,
      this.bottom = 0.0,
      @required this.img,
      this.opacity = 0.0,
      this.landScape});

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: opacity,
      child: Padding(
        padding:
            EdgeInsets.only(top: top, left: left, right: right, bottom: bottom),
        child: Image(
          image: AssetImage(img),
          width: width,
          height: height,
        ),
      ),
    );
  }
}

class ScreenTitle extends StatelessWidget {
  final String title;
  final Color color;
  final double topPadding;
  final double leftPadding;
  final double rightPadding;
  final double bottomPadding;
  final bool landScape;

  ScreenTitle(
      {this.title,
      this.color = Colors.white,
      this.bottomPadding = 0.0,
      this.topPadding = 0.0,
      this.leftPadding = 0.0,
      this.rightPadding = 0.0,
      this.landScape});

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: topPadding),
        alignment: Alignment.topCenter,
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: TextStyle(
              color: color.withOpacity(0.8).withOpacity(0.8),
              fontSize:
                  landScape ? 30 : MediaQuery.of(context).size.width * 0.065),
        ));
  }
}

class CustomDescription extends StatelessWidget {
  final double top, left, right, bottom;
  final String text;
  final TextDirection textDirection;
  final TextAlign textAlign;

  CustomDescription(
      {this.bottom = 0.0,
      this.right = 0.0,
      this.left = 0.0,
      this.top = 0.0,
      this.text = "",
      this.textAlign = TextAlign.center,
      this.textDirection = TextDirection.rtl});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.only(top: top, left: left, right: right, bottom: bottom),
      child: Text(
        text,
        textDirection: textDirection,
        textAlign: textAlign,
        style: TextStyle(color: Colors.white.withOpacity(0.9), fontSize: 17.0),
      ),
    );
  }
}

class ActiveButton extends StatelessWidget {
  final double top, left, right, bottom, width, height;
  final String text;
  Function onPressed;
  final SettingsProvider settingsProvider;

  ActiveButton(
      {this.bottom = 0.0,
      this.top = 0.0,
      this.left = 0.0,
      this.right = 0.0,
      this.text = "",
      this.width = 100.0,
      this.height = 50.0,
      @required this.onPressed,
      this.settingsProvider});

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
        minWidth: width,
        height: height,
        child: Padding(
            padding: EdgeInsets.only(
                top: top, left: left, right: right, bottom: bottom),
            child: FlatButton(
              child: Text(
                text,
                style: TextStyle(color: Colors.white, fontSize: 20.0),
              ),
              color: settingsProvider.color,
              onPressed: onPressed,
            )));
  }
}

class NotifyKeys {
  static const int SABAH_KEY = 0;
  static const int SABAH2_KEY = 1;

  static const int MASAA_KEY = 20;
  static const int MASAA2_KEY = 21;
  static const int MASAA3_KEY = 22;

  static const int FAJR_KEY = 51;
  static const int DUHR_KEY = 52;
  static const int ASR_KEY = 53;
  static const int MEGHRIB_KEY = 54;
  static const int ISHAA_KEY = 55;
  static const int SHROUK_KEY = 56;

  static const int GENERAL = 100;
  static const int GENERAL_SABAH = 101;
  static const int GENERAL_MASAA = 102;
  static const int GENERAL_AZAN = 103;
  static const int TEST = 200;

  static const String ISHAA = "ISHAA";
  static const String DUHR = "DUHR";
  static const String ASR = "ASR";
  static const String MEGHRIB = "MEGHRIB";
  static const String FAJR = "FAJR";
  static const String SHROUK = "SHROUK";

  static const String HOUR = "h";
  static const String MINUTE = "m";

  static const String NO_CITY = "NoCity";
  static const String NO_COUNTRY = "NoCountry";

  static const String NO_INTERNET_CONNECTION =
      "برجاء تفعيل الانترنت للحصول على المواقيت الصحيحة للآذان";

  static const int REPEAT_KEY = 100;
}
