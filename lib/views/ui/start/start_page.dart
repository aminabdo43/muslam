import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/start/location_page.dart';
import 'package:muslam/views/widgets/my_button.dart';
import 'package:provider/provider.dart';

class StartPage extends StatefulWidget {
  @override
  _StartPageState createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  int selectedDuration = 0;
  List<String> durations = ['شهر'];

  int selectedRak3a = -1;
  List<String> rak3at = ['اول ركعتين', 'كل الركعات'];

  int selectedReading = -1;
  List<String> readings = ['تحديد صلوات', 'كل الصلوات'];

  List<int> selected = [
    1,
    2,
    3,
    4,
    5,
  ];

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    return Scaffold(
      appBar: AppBar(
        backgroundColor: settingsProvider.color,
        leading: SizedBox.shrink(),
        actions: [
          FlatButton(
            child: Text(
              'ختمة جديدة',
              style: TextStyle(
                fontSize: 23,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
              textDirection: TextDirection.rtl,
              textAlign: TextAlign.right,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: Builder(
          builder: (context) => Padding(
            padding: EdgeInsets.all(10.0),
            child: Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height,
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'الرجاء  تحديد المدة المراد ختام القران فيها من خلال الخيارات التالية',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      textAlign: TextAlign.right,
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      'مدة الختمة',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      textAlign: TextAlign.right,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: List.generate(
                        durations.length,
                        (index) => myButton(context, durations[index],
                            btnColor: selectedDuration == index
                                ? settingsProvider.color
                                : Colors.grey,
                            width: MediaQuery.of(context).size.width / 4,
                            onTap: () {
                          selectedDuration = index;
                          setState(() {});
                        },
                            textStyle:
                                TextStyle(fontSize: 16, color: Colors.white)),
                      ).reversed.toList(),
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    /*
                    Text(
                      'تحديد الركعات',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      textAlign: TextAlign.right,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: List.generate(
                        rak3at.length,
                        (index) => myButton(context, rak3at[index],
                            btnColor: selectedRak3a == index
                                ? settingsProvider.color
                                : Colors.grey,
                            width: MediaQuery.of(context).size.width / 4,
                            onTap: () {
                          selectedRak3a = index;
                          setState(() {

                          });
                        },
                            textStyle:
                                TextStyle(fontSize: 16, color: Colors.white)),
                      ).reversed.toList(),
                    ),
                    */
                    SizedBox(
                      height: 25,
                    ),
                    Text(
                      'هل ترغب بالقراءة بكل الصلوات',
                      style: TextStyle(fontSize: 18, color: Colors.black),
                      textAlign: TextAlign.right,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: List.generate(
                        readings.length,
                        (index) => myButton(context, readings[index],
                            btnColor: selectedReading == index
                                ? settingsProvider.color
                                : Colors.grey,
                            width: MediaQuery.of(context).size.width / 3,
                            onTap: () {
                          selectedReading = index;
                          setState(() {});
                          if (selectedReading == 0) {
                            _showMultiSelect(context);
                          }
                        },
                            textStyle:
                                TextStyle(fontSize: 16, color: Colors.white)),
                      ).reversed.toList(),
                    ),
                    SizedBox(
                    //  height: MediaQuery.of(context).size.width / 3 - 25,
                      height:  50,
                    ),
                    Center(
                      child: myButton(context, 'تأكيد اعدادات القراءة',
                          btnColor:
                              selectedReading != -1 && selectedDuration != -1
                                  //  && selectedRak3a != -1
                                  ? settingsProvider.color
                                  : Colors.grey,
                          width: MediaQuery.of(context).size.width / 1.8,
                          onTap: selectedReading != -1 && selectedDuration != -1
                              // && selectedRak3a != -1
                              ? () async {
                                  if (selectedReading == 0 && selected.isEmpty) {
                                    showDialog<void>(
                                      context: context,
                                      barrierDismissible: false,
                                      // false = user must tap button, true = tap outside dialog
                                      builder: (BuildContext dialogContext) {
                                        return AlertDialog(
                                          content: Text('برجاء تحديد الصلوات'),
                                          actions: <Widget>[
                                            FlatButton(
                                              child: Text('حسنا'),
                                              onPressed: () {
                                                Navigator.of(dialogContext)
                                                    .pop(); // Dismiss alert dialog
                                              },
                                            ),
                                          ],
                                        );
                                      },
                                    );
                                    return;
                                  }

                                  await AppUtils.saveSelectedRak3a(
                                      selectedRak3a == 0 ? '2' : 'all');
                                  Pointer.selectedRak3a =
                                      selectedRak3a == 0 ? '2' : 'all';

                                  List<String> s = [];
                                  for (int i = 0; i < selected.length; i++) {
                                    s.add(selected[i].toString());
                                  }

                                  await AppUtils.saveReadingSalwat(s);
                                  Pointer.prayes = s;

                                  if (s.length == 5) {
                                    AppUtils.readAllSala(true);
                                  } else {
                                    AppUtils.readAllSala(false);
                                  }

                                  Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (_) => LocationPage(),
                                    ),
                                  );
                                }
                              : null,
                          textStyle:
                              TextStyle(fontSize: 20, color: Colors.white)),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _showMultiSelect(BuildContext context) async {
    final items = <MultiSelectDialogItem<int>>[
      MultiSelectDialogItem(1, 'الفجر'),
      MultiSelectDialogItem(2, 'الظهر'),
      MultiSelectDialogItem(3, 'العصر'),
      MultiSelectDialogItem(4, 'المغرب'),
      MultiSelectDialogItem(5, 'العشاء'),
    ];

    final selectedValues = await showDialog<Set<int>>(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return MultiSelectDialog(
          items: items,
          initialSelectedValues: selected.toSet(),
        );
      },
    );

    selected = selectedValues.toList();
    setState(() {});
  }
}

class MultiSelectDialogItem<V> {
  const MultiSelectDialogItem(this.value, this.label);

  final V value;
  final String label;
}

class MultiSelectDialog<V> extends StatefulWidget {
  MultiSelectDialog({Key key, this.items, this.initialSelectedValues})
      : super(key: key);

  final List<MultiSelectDialogItem<V>> items;
  final Set<V> initialSelectedValues;

  @override
  State<StatefulWidget> createState() => _MultiSelectDialogState<V>();
}

class _MultiSelectDialogState<V> extends State<MultiSelectDialog<V>> {
  final _selectedValues = Set<V>();

  void initState() {
    super.initState();
    if (widget.initialSelectedValues != null) {
      _selectedValues.addAll(widget.initialSelectedValues);
    }
  }

  void _onItemCheckedChange(V itemValue, bool checked) {
    setState(() {
      if (checked) {
        _selectedValues.add(itemValue);
      } else {
        _selectedValues.remove(itemValue);
      }
    });
  }

  void _onCancelTap() {
    Navigator.pop(context);
  }

  void _onSubmitTap() {
    Navigator.pop(context, _selectedValues);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        'اختر الصلوات',
        textAlign: TextAlign.right,
      ),
      contentPadding: EdgeInsets.only(top: 12.0),
      content: SingleChildScrollView(
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: ListTileTheme(
            contentPadding: EdgeInsets.fromLTRB(14.0, 0.0, 24.0, 0.0),
            child: ListBody(
              children: widget.items.map(_buildItem).toList(),
            ),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('الغاء'),
          onPressed: _onCancelTap,
        ),
        FlatButton(
          child: Text('تاكيد'),
          onPressed: _onSubmitTap,
        )
      ],
    );
  }

  Widget _buildItem(MultiSelectDialogItem<V> item) {
    final checked = _selectedValues.contains(item.value);
    return CheckboxListTile(
      value: checked,
      title: Text(item.label),
      controlAffinity: ListTileControlAffinity.leading,
      onChanged: (checked) => _onItemCheckedChange(item.value, checked),
    );
  }
}
