import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/consts.dart';
import 'package:muslam/views/ui/start/start_page.dart';
import 'package:muslam/views/widgets/my_button.dart';
import 'package:provider/provider.dart';

class ChoosePage extends StatefulWidget {
  @override
  _ChoosePageState createState() => _ChoosePageState();
}

class _ChoosePageState extends State<ChoosePage> {
  int currentSelect = 0;

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    return Scaffold(
      body: Container(
        color: settingsProvider.color,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'اختر النوع ليتم تخصيص الشكل',
                    style: TextStyle(
                      fontSize: 25,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: List.generate(
                      2,
                      (index) {
                        return GestureDetector(
                          onTap: () {
                            currentSelect = index;
                            settingsProvider.setColor(
                                currentSelect == 0 ? maleColor : femaleColor);
                            settingsProvider.setGender(
                                currentSelect == 0 ? 'male' : 'female');
                            setState(() {});
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width / 2.7,
                            child: Stack(
                              clipBehavior: Clip.none,
                              children: [
                                AnimatedContainer(
                                  duration: Duration(milliseconds: 500),
                                  padding: EdgeInsets.all(10),
                                  width:
                                      MediaQuery.of(context).size.width / 2.7,
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(25),
                                  ),
                                  child: index == 0 ? male() : female(),
                                ),
                                currentSelect == index
                                    ? Positioned(
                                        right: -5,
                                        top: -14,
                                        child: Container(
                                          width: 40,
                                          height: 40,
                                          decoration: BoxDecoration(
                                            color: greenColor,
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 1.5,
                                            ),
                                            shape: BoxShape.circle,
                                          ),
                                          child: Icon(
                                            Icons.check,
                                            color: Colors.white,
                                            size: 25,
                                          ),
                                        ),
                                      )
                                    : null,
                              ].where((element) => element != null).toList(),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 1,
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.all(18.0),
                  child: myButton(
                    context,
                    'متابعة الان ',
                    onTap: currentSelect == -1
                        ? null
                        : () {
                            AppUtils.saveGender(
                                currentSelect == 0 ? 'male' : 'female');
                            AppUtils.saveColor(currentSelect == 0 ? '0' : '1');
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (_) => StartPage(),
                              ),
                            );
                          },
                    btnColor: currentSelect == -1 ? Colors.grey : Colors.white,
                    textStyle: TextStyle(
                      color: Colors.black,
                      fontSize: 22,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget male() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Image.asset('assets/images/boy.png'),
          Text(
            'ذكر',
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  Widget female() {
    return Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Image.asset('assets/images/woman.png'),
          Text(
            'انثي',
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
