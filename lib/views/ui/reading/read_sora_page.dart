import 'package:dio/dio.dart' as dio;
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:muslam/Bles/model/model/page_model.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/consts.dart';
import 'package:muslam/views/ui/azkar/azkar_page.dart';
import 'package:muslam/views/widgets/swip_to.dart';
import 'package:provider/provider.dart';

class ReadSoraPage extends StatefulWidget {
  final int startPage;
  final int endPage;
  //final String part;
  final String nameOfSelectedSalah;

  final int rak3a1Start;
  final int rak3a1End;

  final int rak3a2Start;
  final int rak3a2End;

  final int rak3a3Start;
  final int rak3a3End;

  final int rak3a4Start;
  final int rak3a4End;

  const ReadSoraPage({
    Key key,
    @required this.startPage,
    @required this.endPage,
    // @required this.part,
    this.nameOfSelectedSalah,
    this.rak3a2Start,
    this.rak3a2End,
    this.rak3a3Start,
    this.rak3a3End,
    this.rak3a4Start,
    this.rak3a4End,
    this.rak3a1Start,
    this.rak3a1End,
  }) : super(key: key);

  @override
  _ReadSoraPageState createState() => _ReadSoraPageState();
}

class _ReadSoraPageState extends State<ReadSoraPage> {
  SettingsProvider settingsProvider = SettingsProvider();
  List<QuraanPageModel> list;
  int numberOfCurrentRak3a;
  int numberOfRak3at;
  String nameOfRak3a;
  double fontSize;
  bool gotData = false;
  double sliderValue = 14;
  bool showSlider;

  int checkNumberOfRak3at() {
    if (widget.nameOfSelectedSalah == "الظهر" ||
        widget.nameOfSelectedSalah == "العصر" ||
        widget.nameOfSelectedSalah == "العشاء") {
      return 2;
    } else if (widget.nameOfSelectedSalah == "المغرب") {
      return 2;
    } else {
      return 2;
    }
  }

  @override
  void initState() {
    numberOfRak3at = checkNumberOfRak3at();
    numberOfCurrentRak3a = 1;
    nameOfRak3a = "الركعة الأولى";
    sliderValue = settingsProvider.fontSize.toDouble();
    showSlider = false;
    super.initState();
    /*
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    */
  }

  Future<List<QuraanPageModel>> getCurrentPartData() async {
    list = List<QuraanPageModel>();
    /*
    for (int i = widget.startPage; i <= widget.endPage; i++) {
      await getContent(i);
    }
*/

    for (int i = widget.rak3a1Start; i <= widget.rak3a1End; i++) {
      await getContent(i);
    }
    return list;
  }

  Future<List<QuraanPageModel>> getCurrentPartData2() async {
    list = List<QuraanPageModel>();
    for (int i = widget.rak3a2Start; i <= widget.rak3a2End; i++) {
      await getContent(i);
    }

    return list;
  }

  Future<List<QuraanPageModel>> getCurrentPartData3() async {
    list = List<QuraanPageModel>();
    for (int i = widget.rak3a3Start; i <= widget.rak3a3End; i++) {
      await getContent(i);
    }

    return list;
  }

  Future<List<QuraanPageModel>> getCurrentPartData4() async {
    list = List<QuraanPageModel>();
    for (int i = widget.rak3a4Start; i <= widget.rak3a4End; i++) {
      await getContent(i);
    }

    return list;
  }

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    if (!gotData) {
      fontSize = settingsProvider.fontSize;
      gotData = true;
    }

    return WillPopScope(
      onWillPop: () async {
        /* SystemChrome.setPreferredOrientations([
          DeviceOrientation.portraitUp,
          DeviceOrientation.portraitDown,
        ]);*/

        setState(() {});
        return true;
      },
      child: Scaffold(
        backgroundColor: BACK_GROUND_COLOR,
        appBar: AppBar(
          backgroundColor: settingsProvider.color,
          actions: [
            FlatButton(
              onPressed: null,
              child: Text(
                nameOfRak3a,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: settingsProvider.fontSize,
                ),
                textAlign: TextAlign.right,
              ),
            ),
            FlatButton(
              onPressed: null,
              child: Text(
                'صلاة ${widget.nameOfSelectedSalah}',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: settingsProvider.fontSize,
                ),
                textAlign: TextAlign.right,
              ),
            ),
            IconButton(
                icon: Icon(Icons.format_size),
                onPressed: () {
                  setState(() {
                    showSlider = !showSlider;
                    /*
                    if (fontSize >= 28) return;
                    fontSize += 2;
                    settingsProvider.setFontSize(fontSize);
                    */
                  });
                })
          ],
          elevation: 0,
        ),
        body: Stack(
          children: [
           // Background(opacity: 0.5),
            ((numberOfRak3at == 2 || numberOfRak3at == 4) &&
                    numberOfCurrentRak3a == 1)
                ? buildFutureBuilder(settingsProvider, getCurrentPartData())
                : ((numberOfRak3at == 2 || numberOfRak3at == 4) &&
                        numberOfCurrentRak3a == 2)
                    ? buildFutureBuilder(
                        settingsProvider, getCurrentPartData2())
                    : (numberOfRak3at == 4 && numberOfCurrentRak3a == 3)
                        ? buildFutureBuilder(
                            settingsProvider, getCurrentPartData3())
                        : buildFutureBuilder(
                            settingsProvider, getCurrentPartData4()),
          ],
        ),
      ),
    );
  }

  Widget buildSlider() {
    return Slider(
        value: sliderValue,
        min: 14,
        max: 28,
        divisions: 7,
        label: sliderValue.toString(),
        onChanged: (value) {
          setState(() {
            sliderValue = value;
            AppUtils.saveFontSize(value);
            fontSize = value;
          });
        });
  }

  FutureBuilder<List<QuraanPageModel>> buildFutureBuilder(
      SettingsProvider settingsProvider,
      Future<List<QuraanPageModel>> futureFun) {
    return FutureBuilder<List<QuraanPageModel>>(
      future: futureFun,
      builder: (context, snapshot) {
        if (snapshot.hasData &&
            snapshot.data != null &&
            snapshot.data.isNotEmpty) {
          var t = '';
          for (int k = 0; k < snapshot.data.length; k++) {
            for (int i = 0; i < snapshot.data[k].data.ayahs.length; i++) {
              t = t +
                  ' ' +
                  snapshot.data[k].data.ayahs[i].text +
                  ' ' +
                  '(${snapshot.data[k].data.ayahs[i].numberInSurah}) ';
            }
          }
          return buildBodyOfAyat(settingsProvider, t, context);
        } else {
          return Stack(
            children: [
              //Background(opacity: 0.5),
              Container(color: BACK_GROUND_COLOR,),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SpinKitRipple(
                    color: Colors.white,
                    size: 70,
                  ),
                  Text(
                    'جاري تحميل محتوي الجزء',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
            ],
          );
        }
      },
    );
  }

  Widget buildBodyOfAyat(
      SettingsProvider settingsProvider, String t, BuildContext context) {
    return Builder(
        builder: (context) => Directionality(
              textDirection: TextDirection.rtl,
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      showSlider ? buildSlider() : Container(),
                      SwipeTo(
                        iconColor: settingsProvider.color,
                        onLeftSwipe: () {
                          setState(() {
                            numberOfCurrentRak3a > 1
                                ? numberOfCurrentRak3a--
                                : numberOfCurrentRak3a = 1;
                            checkNameOfRak3a(numberOfCurrentRak3a);
                          });
                          print(numberOfCurrentRak3a);
                        },
                        onRightSwipe: () {
                          setState(() {
                            numberOfCurrentRak3a < numberOfRak3at
                                ? numberOfCurrentRak3a++
                                : numberOfCurrentRak3a = numberOfRak3at;
                            checkNameOfRak3a(numberOfCurrentRak3a);
                          });
                          print(numberOfCurrentRak3a);
                        },
                        child: SingleChildScrollView(
                          child: Column(
                            children: [
                              Text(
                                '$t',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: fontSize,
                                ),
                              ),
                              ((widget.nameOfSelectedSalah == "الظهر" ||
                                          widget.nameOfSelectedSalah ==
                                              "العصر" ||
                                          widget.nameOfSelectedSalah ==
                                              "العشاء") &&
                                      numberOfCurrentRak3a == 2)
                                  ? buildToAzkarButton()
                                  : (widget.nameOfSelectedSalah == "المغرب" &&
                                          numberOfCurrentRak3a == 2)
                                      ? buildToAzkarButton()
                                      : (widget.nameOfSelectedSalah ==
                                                  "الفجر" &&
                                              numberOfCurrentRak3a == 2)
                                          ? buildToAzkarButton()
                                          : Container()
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  /*
                      child: Text(
                        '$t',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: fontSize,
                        ),
                      ),
                       */
                ),
              ),
            ));
  }

  checkNameOfRak3a(int numberOfRak3a) {
    if (numberOfRak3a == 1) {
      setState(() {
        nameOfRak3a = "الركعة الأولى";
      });
    } else if (numberOfRak3a == 2) {
      setState(() {
        nameOfRak3a = "الركعة الثانية";
      });
    } else if (numberOfRak3a == 3) {
      setState(() {
        nameOfRak3a = "الركعة الثالثة";
      });
    } else {
      setState(() {
        nameOfRak3a = "الركعة الرابعة";
      });
    }
  }

  Widget buildToAzkarButton() {
    return RaisedButton(
        color: maleColor,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 8, 10, 8),
          child: Text(
            "إلى أذكار الصلاة",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
        ),
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
              builder: (_) => AzkarPage(),
            ),
          );
        });
  }

  Future<void> getContent(int i) async {
    QuraanPageModel model = await AppUtils.getPageData(i);
    if (model == null) {
      print('http://api.alquran.cloud/v1/page/$i');
      dio.Response response = await dio.Dio().get(
        'https://api.alquran.cloud/v1/page/$i',
      );
      model = QuraanPageModel.fromJson(response.data);
      AppUtils.savePage(i, model);
    }

    list.add(model);
  }
}

class Background extends StatelessWidget {
  final double opacity;

  Background({this.opacity = 0.0});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/images/background.png'),
          fit: BoxFit.cover,
        ),
      ),
      child: Container(
        decoration: BoxDecoration(color: Colors.black.withOpacity(opacity)),
      ),
    );
  }
}
