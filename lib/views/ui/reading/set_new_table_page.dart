import 'package:flutter/material.dart';
import 'package:muslam/provider/network_provider.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:muslam/views/widgets/my_button.dart';
import 'package:muslam/views/widgets/progress_date_part.dart';
import 'package:provider/provider.dart';

class SetNewTablePage extends StatefulWidget {
  final String type;

  const SetNewTablePage({Key key, this.type}) : super(key: key);

  @override
  _SetNewTablePageState createState() => _SetNewTablePageState();
}

class _SetNewTablePageState extends State<SetNewTablePage> {
  bool useNormalDate = true;
  bool startWithMagreb = true;
  bool readAllSala = true;
  bool startFromNextMonth = true;

  List<String> prays =  [] ;

  SettingsProvider settingsProvider = SettingsProvider();

  List<int> selected = [
    1,
    2,
    3,
    4,
    5,
  ];

  @override
  void initState() {
    super.initState();

    loadSavedDataFromPref();
  }

  void loadSavedDataFromPref() async {
    if (widget.type == null) {
      AppUtils.clearTablePref();
    } else {
      useNormalDate = await AppUtils.getDateHegree() ?? true;
      startWithMagreb = await AppUtils.getMagreebSala() ?? false;
      startFromNextMonth = await AppUtils.getStartSalaFromNextMonth() ?? true;
      readAllSala = Pointer.prayes.length == 5;

      if (!readAllSala) {
        selected.clear();
        for (int i = 0; i < Pointer.prayes.length; i++) {
          selected.add(int.parse(Pointer.prayes[i]));
        }
      }
    }

    setState(() {});
  }

  void onDateTypeSelected(bool val) {
    if (useNormalDate == val) return;
    useNormalDate = val;
    setState(() {});
    /*if (useNormalDate) {
      AppUtils.setDateHegree(true);
    } else {
      AppUtils.setDateHegree(false);
    }*/
  }

  void onNewSalaTypeSelected(bool val) {
    if (startWithMagreb == val) return;
    startWithMagreb = val;
    setState(() {});
    /*if (startWithMagreb) {
      AppUtils.setMagreebSala(true);
    } else {
      AppUtils.setMagreebSala(false);
    }*/
  }

  void onStartFromSelected(bool val) {
    if (startFromNextMonth == val) return;
    startFromNextMonth = val;
    setState(() {});
    /*if (startFromNextMonth) {
      AppUtils.startSalaFromCurrent(true);
    } else {
      AppUtils.startSalaFromCurrent(false);
    }*/
  }

  void _showMultiSelect(BuildContext context) async {
    final items = <MultiSelectDialogItem<int>>[
      MultiSelectDialogItem(1, 'الفجر'),
      MultiSelectDialogItem(2, 'الظهر'),
      MultiSelectDialogItem(3, 'العصر'),
      MultiSelectDialogItem(4, 'المغرب'),
      MultiSelectDialogItem(5, 'العشاء'),
    ];

    final selectedValues = await showDialog<Set<int>>(
      context: context,
      builder: (BuildContext context) {
        return MultiSelectDialog(
          items: items,
          initialSelectedValues: selected.toSet(),
        );
      },
    );

    selected = selectedValues.toList();
    print(selected);
    List<String> s = [];
    for (int i = 0; i < selected.length; i++) {
      s.add('${selected[i]}');
    }

   /* await AppUtils.saveReadingSalwat(s);
    Pointer.prayes = s;*/
    prays = s;
    print(s);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    var networkProvider = Provider.of<NetworkProvider>(context);
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: myAppBar(settingsProvider, 'اعداد جدول قراءة جديد', context),
      body: networkProvider.hasNetworkConnection == null
          ? Center(child: CircularProgressIndicator())
          : !networkProvider.hasNetworkConnection
              ? Center(
                  child: Text('No Internet Connection'),
                )
              : Container(
                  width: double.infinity,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.all(18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: landScape
                                ? EdgeInsets.fromLTRB(MediaQuery.of(context).size.width / 6,
                                2, MediaQuery.of(context).size.width / 6, 2)
                                : const EdgeInsets.all(2.0),
                            child: progressDatePart(settingsProvider, useNormalDate: useNormalDate),
                          ),
                          SizedBox(
                            height: MediaQuery.of(context).size.width / 12,
                          ),
                          Divider(
                            thickness: 2,
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: RadioListTile(
                              value: true,
                              activeColor: settingsProvider.color,
                              toggleable: true,
                              groupValue: useNormalDate,
                              onChanged: onDateTypeSelected,
                              title: Text(
                                'اعتماد الأشهر الهجرية',
                                style: TextStyle(
                                    fontSize: settingsProvider.fontSize),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: RadioListTile(
                              value: false,
                              activeColor: settingsProvider.color,
                              toggleable: true,
                              groupValue: useNormalDate,
                              onChanged: onDateTypeSelected,
                              title: Text(
                                'اعتماد الأشهر الميلادية',
                                style: TextStyle(
                                  fontSize: settingsProvider.fontSize,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Divider(
                            thickness: 2,
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: RadioListTile(
                              value: true,
                              activeColor: settingsProvider.color,
                              toggleable: true,
                              groupValue: startWithMagreb,
                              onChanged: onNewSalaTypeSelected,
                              title: Text(
                                'البدء من صلاة المغرب',
                                style: TextStyle(
                                    fontSize: settingsProvider.fontSize),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: RadioListTile(
                              value: false,
                              toggleable: true,
                              groupValue: startWithMagreb,
                              activeColor: settingsProvider.color,
                              onChanged: onNewSalaTypeSelected,
                              title: Text(
                                'البدء من صلاة الفجر',
                                style: TextStyle(
                                  fontSize: settingsProvider.fontSize,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Divider(
                            thickness: 2,
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: CheckboxListTile(
                              value: readAllSala,
                              activeColor: settingsProvider.color,
                              onChanged: (bool val) {
                                readAllSala = val;
                                setState(() {});
                                AppUtils.readAllSala(true);
                                AppUtils.saveReadingSalwat(
                                    ['1', '2', '3', '4', '5']);
                                Pointer.prayes = ['1', '2', '3', '4', '5'];
                              },
                              title: Text(
                                'القراءة في جميع الصلوات',
                                style: TextStyle(
                                  fontSize: settingsProvider.fontSize,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: CheckboxListTile(
                              value: !readAllSala,
                              onChanged: (bool val) {
                                readAllSala = !val;
                                setState(() {});
                                AppUtils.readAllSala(false);
                                _showMultiSelect(context);
                              },
                              activeColor: settingsProvider.color,
                              title: Text(
                                'تخصيص صلوات',
                                style: TextStyle(
                                  fontSize: settingsProvider.fontSize,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Divider(
                            thickness: 2,
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: RadioListTile(
                              value: true,
                              activeColor: settingsProvider.color,
                              toggleable: true,
                              groupValue: startFromNextMonth,
                              onChanged: onStartFromSelected,
                              title: Text(
                                'البدء من بداية الشهر',
                                style: TextStyle(
                                    fontSize: settingsProvider.fontSize),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          Directionality(
                            textDirection: TextDirection.rtl,
                            child: RadioListTile(
                              value: false,
                              toggleable: true,
                              groupValue: startFromNextMonth,
                              activeColor: settingsProvider.color,
                              onChanged: onStartFromSelected,
                              title: Text(
                                'البدء من الوقت الحالي',
                                style: TextStyle(
                                  fontSize: settingsProvider.fontSize,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                            child: myButton(context, 'اعداد جدول قراءة جديد', width: landScape ? screenWidth*0.6 : null, onTap: () {
                              addNewTable();
                              setState(() {});
                            }),
                          ),
                          SizedBox(
                            height: 30,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }

  addNewTable() async{
    if (useNormalDate) {
      AppUtils.setDateHegree(true);
    } else {
      AppUtils.setDateHegree(false);
    }
    if (startWithMagreb) {
      AppUtils.setMagreebSala(true);
    } else {
      AppUtils.setMagreebSala(false);
    }
    if (startFromNextMonth) {
      AppUtils.startSalaFromCurrent(true);
    } else {
      AppUtils.startSalaFromCurrent(false);
    }
    if(prays.isEmpty){
      Pointer.prayes = ['1' , '2' , '3' , '4' , '5'];
    }else{
      await AppUtils.saveReadingSalwat(prays);
      Pointer.prayes = prays;
    }

    final snackBar = SnackBar(
      content: Padding(
        padding: const EdgeInsets.only(right: 15),
        child: Text(
          'تم اضافة الجدول',
          textDirection: TextDirection.rtl,
          style: TextStyle(color: Colors.white),
        ),
      ),
      backgroundColor: settingsProvider.color,
    );
    ScaffoldMessenger.of(context)
        .showSnackBar(snackBar);
  }
}

class MultiSelectDialogItem<V> {
  const MultiSelectDialogItem(this.value, this.label);

  final V value;
  final String label;
}

class MultiSelectDialog<V> extends StatefulWidget {
  MultiSelectDialog({Key key, this.items, this.initialSelectedValues})
      : super(key: key);

  final List<MultiSelectDialogItem<V>> items;
  final Set<V> initialSelectedValues;

  @override
  State<StatefulWidget> createState() => _MultiSelectDialogState<V>();
}

class _MultiSelectDialogState<V> extends State<MultiSelectDialog<V>> {
  final _selectedValues = Set<V>();

  void initState() {
    super.initState();
    if (widget.initialSelectedValues != null) {
      _selectedValues.addAll(widget.initialSelectedValues);
    }
  }

  void _onItemCheckedChange(V itemValue, bool checked) {
    setState(() {
      if (checked) {
        _selectedValues.add(itemValue);
      } else {
        _selectedValues.remove(itemValue);
      }
    });
  }

  void _onCancelTap() {
    Navigator.pop(context);
  }

  void _onSubmitTap() {
    Navigator.pop(context, _selectedValues);
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        'اختر الصلوات',
        textAlign: TextAlign.right,
      ),
      contentPadding: EdgeInsets.only(top: 12.0),
      content: SingleChildScrollView(
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: ListTileTheme(
            contentPadding: EdgeInsets.fromLTRB(14.0, 0.0, 24.0, 0.0),
            child: ListBody(
              children: widget.items.map(_buildItem).toList(),
            ),
          ),
        ),
      ),
      actions: <Widget>[
        FlatButton(
          child: Text('الغاء'),
          onPressed: _onCancelTap,
        ),
        FlatButton(
          child: Text('تاكيد'),
          onPressed: _onSubmitTap,
        )
      ],
    );
  }

  Widget _buildItem(MultiSelectDialogItem<V> item) {
    final checked = _selectedValues.contains(item.value);
    return CheckboxListTile(
      value: checked,
      title: Text(item.label),
      controlAffinity: ListTileControlAffinity.leading,
      onChanged: (checked) => _onItemCheckedChange(item.value, checked),
    );
  }
}
