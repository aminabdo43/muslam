import 'package:flutter/material.dart';
import 'package:muslam/provider/network_provider.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/consts.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:muslam/views/widgets/my_button.dart';
import 'package:muslam/views/widgets/progress_date_part.dart';
import 'package:provider/provider.dart';

class ShowCurrentTable extends StatefulWidget {
  const ShowCurrentTable({Key key}) : super(key: key);

  @override
  _ShowCurrentTableState createState() => _ShowCurrentTableState();
}

class _ShowCurrentTableState extends State<ShowCurrentTable> {
  bool useNormalDate = true;
  bool startWithMagreb = true;
  bool readAllSala = true;
  bool startFromNextMonth = true;
  List<String> prays;
  bool removeOk;

  List<int> selected = [
    1,
    2,
    3,
    4,
    5,
  ];

  @override
  void initState() {
    super.initState();
    prays = Pointer.prayes;
    loadSavedDataFromPref();
  }

  void loadSavedDataFromPref() async {
    useNormalDate = await AppUtils.getDateHegree() ?? true;
    startWithMagreb = await AppUtils.getMagreebSala() ?? false;
    startFromNextMonth = await AppUtils.getStartSalaFromNextMonth() ?? true;
    readAllSala = Pointer.prayes.length == 5;

    if (!readAllSala) {
      selected.clear();
      for (int i = 0; i < Pointer.prayes.length; i++) {
        selected.add(int.parse(Pointer.prayes[i]));
      }
    }

    setState(() {});
  }

  void onDateTypeSelected(bool val) {
    print(val.toString());
    /* if (useNormalDate == val) return;
    useNormalDate = val;
    setState(() {});
    if (useNormalDate) {
      AppUtils.setDateHegree(true);
    } else {
      AppUtils.setDateHegree(false);
    }*/
  }

  void onNewSalaTypeSelected(bool val) {
    print(val);
    /*if (startWithMagreb == val) return;
    startWithMagreb = val;
    setState(() {});
    if (startWithMagreb) {
      AppUtils.setMagreebSala(true);
    } else {
      AppUtils.setMagreebSala(false);
    }*/
  }

  void onStartFromSelected(bool val) {
    print(val);
    /*if (startFromNextMonth == val) return;
    startFromNextMonth = val;
    setState(() {});
    if (startFromNextMonth) {
      AppUtils.startSalaFromCurrent(true);
    } else {
      AppUtils.startSalaFromCurrent(false);
    }*/
  }

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    var networkProvider = Provider.of<NetworkProvider>(context);
    GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      key: _scaffoldKey,
      appBar:
          myAppBar(settingsProvider, 'استعراض جدول القراءة الحالي', context),
      body: networkProvider.hasNetworkConnection == null
          ? Center(child: CircularProgressIndicator())
          : !networkProvider.hasNetworkConnection
              ? Center(
                  child: Text('No Internet Connection'),
                )
              : Container(
                  width: double.infinity,
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: EdgeInsets.all(18.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: landScape
                                ? EdgeInsets.fromLTRB(
                                    MediaQuery.of(context).size.width / 6,
                                    2,
                                    MediaQuery.of(context).size.width / 6,
                                    2)
                                : const EdgeInsets.all(2.0),
                            child: progressDatePart(settingsProvider,
                                useNormalDate: useNormalDate,
                                landScape: landScape),
                          ),
                          SizedBox(
                            height:
                                landScape ? screenWidth / 20 : screenWidth / 12,
                          ),
                          Divider(
                            thickness: 2,
                          ),
                          useNormalDate
                              ? Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Text(
                                      'اعتماد الأشهر الهجرية',
                                      style: TextStyle(
                                          fontSize: settingsProvider.fontSize),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                )
                              : Container(),
                          useNormalDate
                              ? Container()
                              : Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Text(
                                      'اعتماد الأشهر الميلادية',
                                      style: TextStyle(
                                        fontSize: settingsProvider.fontSize,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                          Divider(
                            thickness: 2,
                          ),
                          startWithMagreb
                              ? Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Text(
                                      'البدء من صلاة المغرب',
                                      style: TextStyle(
                                          fontSize: settingsProvider.fontSize),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                )
                              : Container(),
                          startWithMagreb
                              ? Container()
                              : Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Text(
                                      'البدء من صلاة الفجر',
                                      style: TextStyle(
                                        fontSize: settingsProvider.fontSize,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                          Divider(
                            thickness: 2,
                          ),
                          readAllSala
                              ? Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Text(
                                      'القراءة في جميع الصلوات',
                                      style: TextStyle(
                                        fontSize: settingsProvider.fontSize,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                )
                              : Container(),
                          readAllSala
                              ? Container()
                              : Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Row(
                                      children: [
                                        Text(
                                          prays.contains("1") ? 'الفجر , ' : '',
                                          style: TextStyle(
                                            fontSize: settingsProvider.fontSize,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          prays.contains("2") ? 'الظهر , ' : '',
                                          style: TextStyle(
                                            fontSize: settingsProvider.fontSize,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          prays.contains("3") ? 'العصر , ' : '',
                                          style: TextStyle(
                                            fontSize: settingsProvider.fontSize,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          prays.contains("4")
                                              ? 'المغرب , '
                                              : '',
                                          style: TextStyle(
                                            fontSize: settingsProvider.fontSize,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                        Text(
                                          prays.contains("5") ? 'العشاء' : '',
                                          style: TextStyle(
                                            fontSize: settingsProvider.fontSize,
                                          ),
                                          textAlign: TextAlign.left,
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                          Divider(
                            thickness: 2,
                          ),
                          startFromNextMonth
                              ? Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Text(
                                      'البدء من بداية الشهر',
                                      style: TextStyle(
                                          fontSize: settingsProvider.fontSize),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                )
                              : Container(),
                          startFromNextMonth
                              ? Container()
                              : Directionality(
                                  textDirection: TextDirection.rtl,
                                  child: Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        20, 10, 20, 10),
                                    child: Text(
                                      'البدء من الوقت الحالي',
                                      style: TextStyle(
                                        fontSize: settingsProvider.fontSize,
                                      ),
                                      textAlign: TextAlign.left,
                                    ),
                                  ),
                                ),
                          SizedBox(
                            height: 10,
                          ),
                          Center(
                            child: myButton(context, 'الغاء الجدول الحالي',
                                width: landScape ? screenWidth * 0.6 : null,
                                onTap: () {
                              showMyDialog(context, settingsProvider);
                            }),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
    );
  }

  setDefaultSettings() {
    useNormalDate = true;
    startWithMagreb = true;
    readAllSala = true;
    startFromNextMonth = true;
  }

  cancelCurrentTable(SettingsProvider settingsProvider) {
    AppUtils.clearTablePref();
    setDefaultSettings();
    AppUtils.setMagreebSala(true);
    AppUtils.readAllSala(true);
    AppUtils.saveReadingSalwat(
        ['1', '2', '3', '4', '5']);
    Pointer.prayes = ['1', '2', '3', '4', '5'];
    AppUtils.setDateHegree(true);
    AppUtils.startSalaFromCurrent(false);
    final snackBar = SnackBar(
      content: Padding(
        padding: const EdgeInsets.only(right: 15),
        child: Text(
          'تمت العودة للإعدادات الافتراضية',
          textDirection: TextDirection.rtl,
          style: TextStyle(color: Colors.white),
        ),
      ),
      backgroundColor: settingsProvider.color,
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
    setState(() {
      AppUtils.saveLastPartData(LastPart(partIndex: -1));
    });
    // Navigator.pop(context);
    setState(() {});
  }

  showMyDialog(BuildContext context, SettingsProvider settingsProvider) async {
    return showDialog(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return Directionality(
          textDirection: TextDirection.rtl,
          child: AlertDialog(
            title: Text('هل أنت متأكد؟'),
            content: SingleChildScrollView(
              child: ListBody(
                children: <Widget>[
                  Text('سيؤدي هذا الإجراء إلى العودة للإعدادات الإفتراضية'),
                ],
              ),
            ),
            actions: <Widget>[
              TextButton(
                child: Text('تأكيد'),
                onPressed: () {
                  cancelCurrentTable(settingsProvider);
                  Navigator.of(context).pop();
                },
              ),
              TextButton(
                child: Text('الغاء'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        );
      },
    );
  }
}
