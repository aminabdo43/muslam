import 'package:flutter/material.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/reading/read_sora_page.dart';

class Rak3atPage extends StatefulWidget {
  final int startPage;
  final int endPage;
  final String part;
  final String nameOfSelectedSalah;

  Rak3atPage(
      {Key key,
      this.startPage,
      this.endPage,
      this.part,
      this.nameOfSelectedSalah})
      : super(key: key);

  @override
  _Rak3atPageState createState() => _Rak3atPageState();
}

class _Rak3atPageState extends State<Rak3atPage> {
  int numOfRaq3at = 0;

  @override
  void initState() {
    super.initState();
    rak3arCalc();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => ReadSoraPage(
          nameOfSelectedSalah: widget.nameOfSelectedSalah,
          startPage: rak3a1Start,
          endPage: rak3a1End,
          rak3a2Start: rak3a2Start,
          rak3a2End: rak3a2End,
          rak3a3Start: rak3a2Start,
          rak3a3End: rak3a3End,
          rak3a4Start: rak3a4Start,
          rak3a4End: rak3a4End,
        ),
      ),
    );
  }

  int rak3a1Start;
  int rak3a1End;

  int rak3a2Start;
  int rak3a2End;

  int rak3a3Start;
  int rak3a3End;

  int rak3a4Start;
  int rak3a4End;

  void rak3arCalc() {
    if (Pointer.selectedRak3a == '2' || Pointer.currentPray == 'الفجر') {
      rak3a1Start = widget.startPage;
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 2).ceil();
      rak3a2Start = rak3a1End + 1;
      rak3a2End = widget.endPage;

      print("rak3a1Start $rak3a1Start");
      print("rak3a1End $rak3a1End");
      print("rak3a2Start $rak3a2Start");
      print("rak3a2End $rak3a2End");
      numOfRaq3at = 2;
    } else if (Pointer.currentPray == 'المغرب') {
      rak3a1Start = widget.startPage;
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 3).ceil();
      rak3a2Start = rak3a1End + 1;
      rak3a2End = rak3a2Start + (rak3a1End - rak3a1Start);
      rak3a3Start = rak3a2End + 1;
      rak3a3End = rak3a3Start + (rak3a1End - rak3a1Start);

      print("start page  ${widget.startPage}");
      print("end page ${widget.endPage}");

      print("rak3a1Start $rak3a1Start");
      print("rak3a1End $rak3a1End");
      print("rak3a2Start $rak3a2Start");
      print("rak3a2End $rak3a2End");

      print("rak3a3Start $rak3a3Start");
      print("rak3a3End $rak3a3End");
      numOfRaq3at = 3;
    } else {
      rak3a1Start = widget.startPage;
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 4).ceil();
      rak3a2Start = rak3a1End + 1;
      rak3a2End = rak3a2Start + (rak3a1End - rak3a1Start);
      rak3a3Start = rak3a2End + 1;
      rak3a3End = rak3a3Start + (rak3a1End - rak3a1Start);
      rak3a4Start = rak3a3End + 1;
      rak3a4End = rak3a4Start + (rak3a1End - rak3a1Start);

      print("start page  ${widget.startPage}");
      print("end page ${widget.endPage}");

      print("rak3a1Start $rak3a1Start");
      print("rak3a1End $rak3a1End");
      print("rak3a2Start $rak3a2Start");
      print("rak3a2End $rak3a2End");

      print("rak3a3Start $rak3a3Start");
      print("rak3a3End $rak3a3End");
      print("rak3a4Start $rak3a4Start");
      print("rak3a4End $rak3a4End");
      numOfRaq3at = 4;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container();
    /*
    return Scaffold(
      appBar: myAppBar(
        settingsProvider,
        'الركعات',
        context,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Padding(
        padding: EdgeInsets.all(14.0),
        child: Directionality(
          textDirection: TextDirection.rtl,
          child: GridView.builder(
            itemCount: numOfRaq3at,
            gridDelegate:
                SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {
                  if (index == 0) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => ReadSoraPage(
                          startPage: rak3a1Start,
                          endPage: rak3a1End,
                          part: '${widget.part} - ${index == 0
                              ? 'الركعه الاولي'
                              : index == 1
                              ? 'الركعه الثانيه'
                              : index == 2
                              ? 'الركعه الثالثه'
                              : 'الركعه الرابعه'}',
                        ),
                      ),
                    );
                  } else if (index == 1) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => ReadSoraPage(
                          startPage: rak3a2Start,
                          endPage: rak3a2End,
                          part: '${widget.part} - ${index == 0
                              ? 'الركعه الاولي'
                              : index == 1
                              ? 'الركعه الثانيه'
                              : index == 2
                              ? 'الركعه الثالثه'
                              : 'الركعه الرابعه'}',
                        ),
                      ),
                    );
                  }  else if (index == 3) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => ReadSoraPage(
                          startPage: rak3a3Start,
                          endPage: rak3a3End,
                          part: '${widget.part} - ${index == 0
                              ? 'الركعه الاولي'
                              : index == 1
                              ? 'الركعه الثانيه'
                              : index == 2
                              ? 'الركعه الثالثه'
                              : 'الركعه الرابعه'}',
                        ),
                      ),
                    );
                  }  else if (index == 4) {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => ReadSoraPage(
                          startPage: rak3a4Start,
                          endPage: rak3a4End,
                          part: '${widget.part} - ${index == 0
                              ? 'الركعه الاولي'
                              : index == 1
                              ? 'الركعه الثانيه'
                              : index == 2
                              ? 'الركعه الثالثه'
                              : 'الركعه الرابعه'}',
                        ),
                      ),
                    );
                  }
                },
                child: Card(
                  child: Center(
                    child: Text(
                      index == 0
                          ? 'الركعه الاولي'
                          : index == 1
                              ? 'الركعه الثانيه'
                              : index == 2
                                  ? 'الركعه الثالثه'
                                  : 'الركعه الرابعه',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
    */
  }
}
