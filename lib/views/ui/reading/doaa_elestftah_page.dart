import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/reading/read_sora_page.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:muslam/views/widgets/my_button.dart';
import 'package:provider/provider.dart';

class DoaaElestftahPage extends StatefulWidget {
  final int startPage;
  final int endPage;
  final String part;
  final String nameOfSelectedSalah;

  DoaaElestftahPage({
    Key key,
    @required this.startPage,
    @required this.endPage,
    @required this.part,
    this.nameOfSelectedSalah,
  }) : super(key: key);

  @override
  _DoaaElestftahPageState createState() => _DoaaElestftahPageState();
}

class _DoaaElestftahPageState extends State<DoaaElestftahPage> {
  int numOfRaq3at = 0;
  bool select = true;

  @override
  void initState() {
    super.initState();
    rak3arCalc();
  }

  int rak3a1Start;
  int rak3a1End;

  int rak3a2Start;
  int rak3a2End;

  int rak3a3Start;
  int rak3a3End;

  int rak3a4Start;
  int rak3a4End;

  void rak3arCalc() async {
    print(widget.startPage);

    print(widget.endPage);

    print(widget.part);

    print('rak3arCalc ---> prayers end : ${Pointer.prayes.toString()}');

    Pointer.prayes.forEach((element) {
      print(element);
    });

    //if (Pointer.selectedRak3a == '2' || Pointer.currentPray == 'الفجر')
    if (widget.nameOfSelectedSalah == 'الفجر') {
      rak3a1Start = widget.startPage;
      /*
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 4).ceil();*/

      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 2).ceil();
      rak3a2Start = rak3a1End + 1;
      rak3a2End = rak3a2Start + (rak3a1End - rak3a1Start);
      rak3a3Start = rak3a2End + 1;
      /*
      rak3a3End = rak3a3Start + (rak3a2End - rak3a2Start);
      rak3a4Start = rak3a3End + 1;
      rak3a4End = rak3a4Start + (rak3a3End - rak3a3Start);
      */

      print("rak3a1Start $rak3a1Start");
      print("rak3a1End $rak3a1End");
      print("rak3a2Start $rak3a2Start");
      print("rak3a2End $rak3a2End");
      /*
      print("rak3a3Start $rak3a3Start");
      print("rak3a3End $rak3a3End");
      print("rak3a4Start $rak3a4Start");
      print("rak3a4End $rak3a4End");
      */
      numOfRaq3at = 2;
      // numOfRaq3at = 4;
    } else if (widget.nameOfSelectedSalah == 'المغرب') {
      rak3a1Start = widget.startPage;
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 2).ceil();
      /*
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 3).ceil();
       */
      rak3a2Start = rak3a1End + 1;
      rak3a2End = rak3a2Start + (rak3a1End - rak3a1Start);

      print("start page  ${widget.startPage}");
      print("end page ${widget.endPage}");
      print("rak3a1Start $rak3a1Start");
      print("rak3a1End $rak3a1End");
      print("rak3a2Start $rak3a2Start");
      print("rak3a2End $rak3a2End");

      //numOfRaq3at = 3;
      numOfRaq3at = 2;
    } else {
      rak3a1Start = widget.startPage;
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 2).ceil();
      /*
      rak3a1End =
          rak3a1Start + ((widget.endPage - widget.startPage) / 4).ceil();
       */
      rak3a2Start = rak3a1End + 1;
      rak3a2End = rak3a2Start + (rak3a1End - rak3a1Start);

      print("start page  ${widget.startPage}");
      print("end page ${widget.endPage}");

      print("rak3a1Start $rak3a1Start");
      print("rak3a1End $rak3a1End");
      print("rak3a2Start $rak3a2Start");
      print("rak3a2End $rak3a2End");

      //numOfRaq3at = 4;
      numOfRaq3at = 2;
    }
  }

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);

    return Scaffold(
      appBar: myAppBar(
        settingsProvider,
        'دعاء الاستفتاح',
        context,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.only(top: 4, bottom: 4, right: 10, left: 10),
              child: FutureBuilder(
                future: AppUtils.getDo3aaAlestftah(),
                builder: (context, snap) {
                  if(snap.hasData){
                    return Text(
                      snap.data.toString() ?? "سبحانك اللهم وبحمدك وتبارك اسمك وتعالى جدك ولا إله غيرك",
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: settingsProvider.fontSize,
                      ),
                    );
                  }else{
                    return Text(
                      "سبحانك اللهم وبحمدك وتبارك اسمك وتعالى جدك ولا إله غيرك",
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontSize: settingsProvider.fontSize,
                      ),
                    );
                  }
                },
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Center(
              child: myButton(
                context,
                'بدء القراءة',
                width: MediaQuery.of(context).size.width * .85,
                onTap: () {
                  Navigator.pushReplacement(
                    context,
                    MaterialPageRoute(
                      builder: (_) => ReadSoraPage(
                        nameOfSelectedSalah: widget.nameOfSelectedSalah,
                        startPage: widget.startPage,
                        endPage: widget.endPage,
                        rak3a1Start: rak3a1Start,
                        rak3a1End: rak3a1End,
                        rak3a2Start: rak3a2Start,
                        rak3a2End: rak3a2End,
                        rak3a3Start: rak3a3Start,
                        rak3a3End: rak3a3End,
                        rak3a4Start: rak3a4Start,
                        rak3a4End: rak3a4End,
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
