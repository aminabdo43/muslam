import 'package:flutter/material.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/consts.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/reading/display_salawat_page.dart';
import 'package:muslam/views/ui/reading/set_new_table_page.dart';
import 'package:muslam/views/ui/reading/show_current_table.dart';
import 'package:muslam/views/ui/settings/settings_page.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:muslam/views/widgets/my_button.dart';
import 'package:muslam/views/widgets/progress_date_part.dart';
import 'package:provider/provider.dart';

class ReadingPage extends StatefulWidget {
  @override
  _ReadingPageState createState() => _ReadingPageState();
}

class _ReadingPageState extends State<ReadingPage> {
  bool useNormalDate;
  @override
  void initState() {
    getData();
    super.initState();
  }

  getData() async {
    useNormalDate = await AppUtils.getDateHegree() ?? false;
  }

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: myAppBar(settingsProvider, 'القراءة', context),
      body: Container(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.all(8.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: landScape ?CrossAxisAlignment.center : CrossAxisAlignment.end,
              children: [
                Padding(
                  padding: landScape
                      ? EdgeInsets.fromLTRB(MediaQuery.of(context).size.width / 6,
                          2, MediaQuery.of(context).size.width / 6, 2)
                      : const EdgeInsets.all(2.0),
                  child: progressDatePart(settingsProvider,
                      useNormalDate: useNormalDate, landScape: landScape),
                ),
                SizedBox(
                  height: landScape ?MediaQuery.of(context).size.width / 20 :MediaQuery.of(context).size.width / 4,
                ),
                myButton(
                  context,
                  'بدء الصلاة',
                  width: landScape ? screenWidth*0.6 : null ,
                  btnColor: settingsProvider.color,
                  onTap: () async {
                    String part;
                    int start;
                    int end;
                    DateTime dateNow = DateTime.now();
                    // first time
                    if (Pointer.lastPart == null) {
                      print('first time');
                      // point to first part
                      part = parts[0].name;
                      start = parts[0].start;
                      end = parts[0].end;

                      // save as first part
                      AppUtils.saveLastPartData(LastPart(
                        date: '${dateNow.day}/${dateNow.month}/${dateNow.year}',
                        partIndex: 0,
                      ));
                    } else {
                      print('in the same day');
                      // in the same day
                      if (Pointer.lastPart.date ==
                          '${dateNow.day}/${dateNow.month}/${dateNow.year}') {
                        part = parts[Pointer.lastPart.partIndex].name;
                        start = parts[Pointer.lastPart.partIndex].start;
                        end = parts[Pointer.lastPart.partIndex].end;

                        // a new day
                      } else {
                        print('the parts not ended yet (has more parts to read)');
                        // the parts not ended yet (has more parts to read)
                        if (Pointer.lastPart.partIndex + 1 != parts.length - 1) {
                          part = parts[Pointer.lastPart.partIndex + 1].name;
                          start = parts[Pointer.lastPart.partIndex + 1].start;
                          end = parts[Pointer.lastPart.partIndex + 1].end;
                        } else {
                          await showDialog<void>(
                            context: context,
                            barrierDismissible: true,
                            // false = user must tap button, true = tap outside dialog
                            builder: (BuildContext dialogContext) {
                              return AlertDialog(
                                title: Text(
                                  'تهانينا',
                                  textAlign: TextAlign.right,
                                  textDirection: TextDirection.rtl,
                                ),
                                content: Text(
                                  'لقد اكملت قراءه كل اجزاء القرأن الكريم كاملا\nهل ترغب بالبدء من جديد',
                                  textAlign: TextAlign.right,
                                  textDirection: TextDirection.rtl,
                                ),
                                actions: <Widget>[
                                  FlatButton(
                                    child: Text(
                                      'لا',
                                      textAlign: TextAlign.right,
                                      textDirection: TextDirection.rtl,
                                    ),
                                    onPressed: () {
                                      Navigator.of(dialogContext).pop();
                                    },
                                  ),
                                  FlatButton(
                                    child: Text(
                                      'نعم',
                                      textAlign: TextAlign.right,
                                      textDirection: TextDirection.rtl,
                                    ),
                                    onPressed: () {
                                      AppUtils.saveLastPartData(LastPart(
                                        date:
                                            '${dateNow.day}/${dateNow.month}/${dateNow.year}',
                                        partIndex: 0,
                                      ));

                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (_) => DisplaySlwatPage(
                                            part: 'الجزء الاول',
                                            endPage: parts[0].end,
                                            startPage: parts[0].start,
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                ],
                              );
                            },
                          );

                          return;
                        }
                      }
                    }

                    Pointer.lastPart = await AppUtils.getLastPartData();
                    print(Pointer.lastPart?.toMap());

                    // save the new date
                    AppUtils.saveLastPartData(
                      LastPart(
                        partIndex: Pointer.lastPart.partIndex + 1,
                        date: '${dateNow.day}/${dateNow.month}/${dateNow.year}',
                      ),
                    );

                    Navigator.of(context)
                        .push(
                      MaterialPageRoute(
                        builder: (_) => DisplaySlwatPage(
                          part: part,
                          endPage: end,
                          startPage: start,
                        ),
                      ),
                    )
                        .then(
                      (value) {
                        if (mounted) setState(() {});
                      },
                    );
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width / 20,
                ),
                myButton(
                  context,
                  'استعراض جدول القراءة الحالي',
                  width: landScape ? screenWidth*0.6 : null ,
                  btnColor: settingsProvider.color,
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        //  builder: (_) => SetNewTablePage(type: 'show',),
                        builder: (_) => ShowCurrentTable(),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width / 20,
                ),
                myButton(
                  context,
                  'اعداد جدول قراءة جديد',
                  width: landScape ? screenWidth*0.6 : null ,
                  btnColor: settingsProvider.color,
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => SetNewTablePage(),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.width / 20,
                ),
                myButton(
                  context,
                  'الاعدادات',
                  width: landScape ? screenWidth*0.6 : null ,
                  btnColor: settingsProvider.color,
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => SettingsPage(),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
