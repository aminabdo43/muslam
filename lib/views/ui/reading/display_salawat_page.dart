import 'package:flutter/material.dart';
import 'package:muslam/provider/network_provider.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/reading/doaa_elestftah_page.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:provider/provider.dart';

class DisplaySlwatPage extends StatefulWidget {
  final int startPage;
  final int endPage;
  final String part;

  const DisplaySlwatPage({Key key, this.startPage, this.endPage, this.part})
      : super(key: key);

  @override
  _DisplaySlwatPageState createState() => _DisplaySlwatPageState();
}

class _DisplaySlwatPageState extends State<DisplaySlwatPage> {
  @override
  initState() {
    super.initState();

    print('input start: ${widget.startPage}');
    print('input end : ${widget.endPage}');
    print('prayers end : ${Pointer.prayes.toString()}');
  }

  @override
  Widget build(BuildContext context) {
    var networkProvider = Provider.of<NetworkProvider>(context);
    var settingsProvider = Provider.of<SettingsProvider>(context);
    bool landScape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: myAppBar(
        settingsProvider,
        'الصلوات المختارة',
        context,
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: networkProvider.hasNetworkConnection == null
          ? Center(child: CircularProgressIndicator())
          : !networkProvider.hasNetworkConnection
              ? Center(
                  child: Text('No Internet Connection'),
                )
              : SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          'حان الان موعد صلاة ${Pointer.currentPray == "الشروق" ? "الفجر" : Pointer.currentPray}',
                          style: TextStyle(
                            fontSize: settingsProvider.fontSize,
                            color: settingsProvider.color,
                          ),
                        ),
                      ),
                      Directionality(
                        textDirection: TextDirection.rtl,
                        child: Padding(
                          padding: landScape ? EdgeInsets.fromLTRB(screenWidth*0.2, 0, screenWidth*0.2, 100):EdgeInsets.all(0),
                          child: GridView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              mainAxisSpacing: 5,
                              crossAxisSpacing: 5,
                              crossAxisCount: 2,
                              childAspectRatio: 1,
                            ),
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  goToReadSoraPage(index);
                                },
                                child: Padding(
                                  padding: EdgeInsets.all(9.0),
                                  child: Material(
                                    borderRadius: BorderRadius.circular(8),
                                    color: settingsProvider.color,
                                    elevation: 4,
                                    child: Padding(
                                      padding: EdgeInsets.all(8.0),
                                      child: Center(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: [
                                            Text(
                                              'صلاة',
                                              style: TextStyle(
                                                fontSize:
                                                    settingsProvider.fontSize,
                                                color: Colors.white,
                                              ),
                                            ),
                                            Text(
                                              Pointer.prayes[index] == '1'
                                                  ? 'الفجر'
                                                  : Pointer.prayes[index] == '2'
                                                      ? 'الظهر'
                                                      : Pointer.prayes[index] ==
                                                              '3'
                                                          ? 'العصر'
                                                          : Pointer.prayes[
                                                                      index] ==
                                                                  '4'
                                                              ? 'المغرب'
                                                              : 'العشاء',
                                              style: TextStyle(
                                                fontSize:
                                                    settingsProvider.fontSize,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                            itemCount: Pointer.prayes?.length ?? 0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }

  void goToReadSoraPage(int index) {
    int start, end;
    String salahName;
    // prayes  =>> عدد الصلوات الي اختارها المستخدم
    // widget.startPage  =>> رقم بدايه الجزء
    // widget.endPage  =>> رقم نهايه الجزء
    // index  => رقم الصلاه الحاليه

    if (Pointer.prayes[index] == '1') {
      salahName = 'الفجر';
    } else if (Pointer.prayes[index] == '2') {
      salahName = 'الظهر';
    } else if (Pointer.prayes[index] == '3') {
      salahName = 'العصر';
    } else if (Pointer.prayes[index] == '4') {
      salahName = 'المغرب';
    } else if (Pointer.prayes[index] == '5') {
      salahName = 'العشاء';
    }

    if (Pointer.prayes.length == 1) {
      start = widget.startPage;
      end = widget.endPage;
      print('>>> prayes length: 1');
      print('>>> start ${widget.startPage}');
      print('>>> end ${widget.endPage}');
    } else if (Pointer.prayes.length == 2) {
      int numPages = ((widget.endPage - widget.startPage) / 5).ceil();
      print("index >>>>>>>> $index");
      if (widget.startPage == 1 ||
          widget.startPage == 30 ||
          widget.startPage == 0) {
        numPages = 11;
      } else {
        numPages = 10;
      }

      if (index == 1) {
        start = widget.startPage;
        end = start + numPages;
      } else if (index == 2) {
        start = widget.startPage + (numPages);
        end = start + numPages;
      } else if (index == 3) {
        start = widget.startPage + (2 * numPages);
        end = start + numPages;
      } else if (index == 4) {
        start = widget.startPage + (3 * numPages);
        end = start + numPages;
      } else {
        start = widget.startPage + (4 * numPages);
        end = start + numPages;
      }

      print('>>> prayes length: 5');
      print('>>> start $start');
      print('>>> end $end');
    } else if (Pointer.prayes.length == 3) {
      int numPages = ((widget.endPage - widget.startPage) / 5).ceil();
      print("index >>>>>>>> $index");
      if (widget.startPage == 1 ||
          widget.startPage == 30 ||
          widget.startPage == 0) {
        numPages = 7;
      } else {
        numPages = 6;
      }

      if (index == 1) {
        start = widget.startPage;
        end = start + numPages;
      } else if (index == 2) {
        start = widget.startPage + (numPages);
        end = start + numPages;
      } else if (index == 3) {
        start = widget.startPage + (2 * numPages);
        end = start + numPages;
      } else if (index == 4) {
        start = widget.startPage + (3 * numPages);
        end = start + numPages;
      } else {
        start = widget.startPage + (4 * numPages);
        end = start + numPages;
      }

      print('>>> prayes length: 5');
      print('>>> start $start');
      print('>>> end $end');
    } else if (Pointer.prayes.length == 4) {
      int numPages = ((widget.endPage - widget.startPage) / 5).ceil();
      print("index >>>>>>>> $index");
      if (widget.startPage == 1 ||
          widget.startPage == 30 ||
          widget.startPage == 0) {
        numPages = 5;
      } else {
        numPages = 5;
      }

      if (index == 1) {
        start = widget.startPage;
        end = start + numPages;
      } else if (index == 2) {
        start = widget.startPage + (numPages);
        end = start + numPages;
      } else if (index == 3) {
        start = widget.startPage + (2 * numPages);
        end = start + numPages;
      } else if (index == 4) {
        start = widget.startPage + (3 * numPages);
        end = start + numPages;
      } else {
        start = widget.startPage + (4 * numPages);
        end = start + numPages;
      }

      print('>>> prayes length: 5');
      print('>>> start $start');
      print('>>> end $end');
    } else if (Pointer.prayes.length == 5) {
      int numPages = ((widget.endPage - widget.startPage) / 5).ceil();
      print("index >>>>>>>> $index");
      if (widget.startPage == 1 ||
          widget.startPage == 30 ||
          widget.startPage == 0) {
        numPages = 4;
      } else {
        numPages = 4;
      }

      if (index == 1) {
        start = widget.startPage;
        end = start + numPages;
      } else if (index == 2) {
        start = widget.startPage + (numPages);
        end = start + numPages;
      } else if (index == 3) {
        start = widget.startPage + (2 * numPages);
        end = start + numPages;
      } else if (index == 4) {
        start = widget.startPage + (3 * numPages);
        end = start + numPages;
      } else {
        start = widget.startPage + (4 * numPages);
        end = start + numPages;
      }

      print('>>> prayes length: 5');
      print('>>> start $start');
      print('>>> end $end');
    }

    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => DoaaElestftahPage(
          nameOfSelectedSalah: salahName,
          startPage: start,
          endPage: end,
          part: widget.part,
        ),
      ),
    );
  }
}
