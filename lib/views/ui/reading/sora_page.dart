import 'package:flutter/material.dart';
import 'package:muslam/provider/network_provider.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/views/ui/reading/display_salawat_page.dart';
import 'package:muslam/views/widgets/my_app_bar.dart';
import 'package:provider/provider.dart';

class SoraPage extends StatefulWidget {
  @override
  _SoraPageState createState() => _SoraPageState();
}

class _SoraPageState extends State<SoraPage> {
  List<Part> parts = [
    Part('الجزء الاول', 1, 21),
    Part('الجزء الثاني', 22, 41),
    Part('الجزء الثالث', 42, 61),
    Part('الجزء الرابع', 62, 81),
    Part('الجزء الخامس', 82, 101),
    Part('الجزء السادس', 102, 121),
    Part('الجزء السابع', 122, 141),
    Part('الجزء الثامن', 142, 161),
    Part('الجزء التاسع', 162, 181),
    Part('الجزء العاشر', 182, 201),
    Part('الجزء الحادي عشر', 202, 221),
    Part('الجزء الثاني عشر', 222, 241),
    Part('الجزء الثالث عشر', 242, 261),
    Part('الجزء الرابع عشر', 262, 281),
    Part('الجزء الخامس عشر', 282, 301),
    Part('الجزء السادس عشر', 302, 321),
    Part('الجزء السابع عشر', 322, 341),
    Part('الجزء الثامن عشر', 342, 361),
    Part('الجزء التاسع عشر', 362, 381),
    Part('الجزء العشرون', 382, 401),
    Part('الجزء الحادي والعشرون', 402, 421),
    Part('الجزء الثاني والعشرون', 422, 441),
    Part('الجزء الثالث والعشرون', 442, 461),
    Part('الجزء الرابع والعشرون', 462, 481),
    Part('الجزء الخامس والعشرون', 482, 501),
    Part('الجزء السادس والعشرون', 502, 521),
    Part('الجزء السابع والعشرون', 522, 541),
    Part('الجزء الثامن والعشرون', 542, 561),
    Part('الجزء التاسع والعشرون', 562, 581),
    Part('الجزء الثلاثون', 582, 604),
  ];

  @override
  Widget build(BuildContext context) {
    var settingsProvider = Provider.of<SettingsProvider>(context);
    var networkProvider = Provider.of<NetworkProvider>(context);

    return Scaffold(
      appBar: myAppBar(settingsProvider, 'القراءة', context),
      body: networkProvider.hasNetworkConnection == null
          ? Center(child: CircularProgressIndicator())
          : !networkProvider.hasNetworkConnection
              ? Center(
                  child: Text('No Internet Connection'),
                )
              : ListView.separated(
                  itemBuilder: (context, index) {
                    return Directionality(
                      textDirection: TextDirection.rtl,
                      child: ListTile(
                        leading: Icon(
                          Icons.circle,
                          color: settingsProvider.color,
                          size: 15,
                        ),
                        title: Text(parts[index].name),
                        subtitle: Text(index == 0 ? '22 صفحه' : '20 صفحه'),
                        onTap: () {
                          print('>>>>>>> ${parts[index].start}');
                          print('>>>>>>> ${parts[index].end}');
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => DisplaySlwatPage(
                                startPage: parts[index].start,
                                endPage: parts[index].end,
                                part: parts[index].name,
                              ),
                            ),
                          );
                        },
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider();
                  },
                  itemCount: parts.length,
                ),
    );
  }
}

class Part {
  String name;
  int start;
  int end;

  Part(this.name, this.start, this.end);
}
