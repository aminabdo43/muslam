import 'package:flutter/material.dart';

final String appName = 'Muslam';

Color maleColor = Color(0xff3C3CD1);
Color femaleColor = Color(0xffA91D55);
Color greenColor = Color(0xff01AA99);
const Color BACK_GROUND_COLOR = Color(0xff000a12);

final double defaultTextSize = 14;

List<Part> parts = [
  Part('الجزء الاول', 2, 21),
  Part('الجزء الثاني', 22, 41),
  Part('الجزء الثالث', 42, 61),
  Part('الجزء الرابع', 62, 81),
  Part('الجزء الخامس', 82, 101),
  Part('الجزء السادس', 102, 121),
  Part('الجزء السابع', 122, 141),
  Part('الجزء الثامن', 142, 161),
  Part('الجزء التاسع', 162, 181),
  Part('الجزء العاشر', 182, 201),
  Part('الجزء الحادي عشر', 202, 221),
  Part('الجزء الثاني عشر', 222, 241),
  Part('الجزء الثالث عشر', 242, 261),
  Part('الجزء الرابع عشر', 262, 281),
  Part('الجزء الخامس عشر', 282, 301),
  Part('الجزء السادس عشر', 302, 321),
  Part('الجزء السابع عشر', 322, 341),
  Part('الجزء الثامن عشر', 342, 361),
  Part('الجزء التاسع عشر', 362, 381),
  Part('الجزء العشرون', 382, 401),
  Part('الجزء الحادي والعشرون', 402, 421),
  Part('الجزء الثاني والعشرون', 422, 441),
  Part('الجزء الثالث والعشرون', 442, 461),
  Part('الجزء الرابع والعشرون', 462, 481),
  Part('الجزء الخامس والعشرون', 482, 501),
  Part('الجزء السادس والعشرون', 502, 521),
  Part('الجزء السابع والعشرون', 522, 541),
  Part('الجزء الثامن والعشرون', 542, 561),
  Part('الجزء التاسع والعشرون', 562, 581),
  Part('الجزء الثلاثون', 582, 604),
];

class Part {
  String name;
  int start;
  int end;

  Part(this.name, this.start, this.end);
}

class LastPart {
  String date;
  int partIndex;

  LastPart({this.date, this.partIndex});

  LastPart.fromMap(Map<String, dynamic> map)
      : date = map['date'],
        partIndex = map['partIndex'];

  Map toMap() {
    return {
      'date': this.date,
      'partIndex': this.partIndex,
    };
  }
}
