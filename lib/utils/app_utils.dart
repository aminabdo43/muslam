import 'dart:convert';

import 'package:muslam/Bles/model/model/page_model.dart';
import 'package:muslam/utils/consts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppUtils {
  static Future<void> saveGender(String gender) async {
    (await SharedPreferences.getInstance()).setString('gender', gender);
  }

  // all | 2
  static Future<void> saveSelectedRak3a(String selectedRak3a) async {
    (await SharedPreferences.getInstance())
        .setString('selectedRak3a', selectedRak3a);
  }

  // list of selected salwat
  static Future<void> saveReadingSalwat(List<String> prayes) async {
    (await SharedPreferences.getInstance()).setStringList('prayes', prayes);
  }

  static Future<void> saveUserLocationLatitude(double lat) async {
    (await SharedPreferences.getInstance()).setDouble('lat', lat);
  }

  static Future<void> saveUserLocationLong(double long) async {
    (await SharedPreferences.getInstance()).setDouble('long', long);
  }

  static Future<void> saveColor(String color) async {
    (await SharedPreferences.getInstance()).setString('color', color);
  }

  // method to save last Part data
  static saveLastPartData(LastPart part) {
    SharedPreferences.getInstance().then((pref) {
      String data = jsonEncode(part.toMap());
      pref.setString('lastPart', data);
    });
  }

  // method to load last part data
  static Future<LastPart> getLastPartData() async {
    LastPart lastPart;
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString('lastPart') == null) {
      return null;
    }
    Map<String, dynamic> lastPartData = json.decode(pref.getString('lastPart'));
    lastPart = LastPart.fromMap(lastPartData);
    return lastPart;
  }

  static Future<void> saveFontSize(double size) async {
    (await SharedPreferences.getInstance()).setDouble('fontSize', size);
  }

  static Future<void> setDateHegree(bool setHegree) async {
    (await SharedPreferences.getInstance()).setBool('setDateHegree', setHegree);
  }

  static Future<void> setMagreebSala(bool setMagreebSala) async {
    (await SharedPreferences.getInstance())
        .setBool('setMagreebSala', setMagreebSala);
  }

  static Future<void> saveIsFirstTimeLaunch() async {
    (await SharedPreferences.getInstance()).setBool('isFirstTimeLaunch', false);
  }

  static Future<void> readAllSala(bool readAllSala) async {
    (await SharedPreferences.getInstance()).setBool('readAllSala', readAllSala);
  }

  static Future<void> startSalaFromCurrent(bool startSalaFromCurrent) async {
    (await SharedPreferences.getInstance())
        .setBool('startSalaFromCurrent', startSalaFromCurrent);
  }

  static Future<String> getSavedGender() async {
    return (await SharedPreferences.getInstance()).getString('gender');
  }

  static Future<String> getSavedColor() async {
    return (await SharedPreferences.getInstance()).getString('color');
  }

  static Future<double> getSavedFontSize() async {
    return (await SharedPreferences.getInstance()).getDouble('fontSize');
  }

  static Future<bool> getIsFirstTimeLaunch() async {
    return (await SharedPreferences.getInstance()).getBool('isFirstTimeLaunch');
  }

  static Future<bool> getMagreebSala() async {
    return (await SharedPreferences.getInstance()).getBool('setMagreebSala');
  }

  static Future<bool> getDateHegree() async {
    return (await SharedPreferences.getInstance()).getBool('setDateHegree');
  }

  static Future<bool> getStartSalaFromNextMonth() async {
    return (await SharedPreferences.getInstance())
        .getBool('startSalaFromCurrent');
  }

  static Future<String> getDo3aaAlestftah() async {
    return (await SharedPreferences.getInstance()).getString('savedDo3aa');
  }

  static Future<void> setDo3aaAlestftah(String selectedDo3aa) async {
    (await SharedPreferences.getInstance())
        .setString('savedDo3aa', selectedDo3aa);
  }

  static Future<bool> getReadAllSala() async {
    return (await SharedPreferences.getInstance()).getBool('readAllSala');
  }

  static Future<String> getSelectedRak3a() async {
    return (await SharedPreferences.getInstance()).getString('selectedRak3a');
  }

  // list of selected salwat
  static Future<List<String>> getReadingSalwat() async {
    return ((await SharedPreferences.getInstance()))
        .getStringList('prayes');
  }

  static Future<double> getUserLocationLatitude() async {
    return (await SharedPreferences.getInstance()).getDouble('lat');
  }

  static Future<double> getUserLocationLong() async {
    return (await SharedPreferences.getInstance()).getDouble('long');
  }

  static clearTablePref() async {
    (await SharedPreferences.getInstance()).remove('readAllSala');
    (await SharedPreferences.getInstance()).remove('startSalaFromCurrent');
    (await SharedPreferences.getInstance()).remove('setDateHegree');
    (await SharedPreferences.getInstance()).remove('setMagreebSala');
  }

  static savePage(int i, QuraanPageModel model) async {
    SharedPreferences.getInstance().then((pref) {
      String data = jsonEncode(model);
      pref.setString('aya$i', data);
    });
  }

  static Future<QuraanPageModel> getPageData(int i) async {
    QuraanPageModel model;
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (pref.getString('aya$i') == null) {
      return null;
    }
    Map<String, dynamic> modelMap = json.decode(pref.getString('aya$i'));
    if (modelMap == null) {
      return null;
    }
    model = QuraanPageModel.fromJson(modelMap);
    return model;
  }
}
