
import 'package:flutter/material.dart';

import 'consts.dart';

class Pointer {
  static String gender;
  static Color color;
  static bool isFirstTimeLaunch;
  static double fontSize;
  static double lat;
  static double long;
  static List<String> prayes;
  static String selectedRak3a;
  static String currentPray;
  static LastPart lastPart;
}