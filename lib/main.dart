import 'package:adhan_flutter/adhan_flutter.dart';
import 'package:flutter/material.dart';
import 'package:muslam/provider/network_provider.dart';
import 'package:muslam/provider/page_provider.dart';
import 'package:muslam/provider/settings_providers.dart';
import 'package:muslam/utils/app_utils.dart';
import 'package:muslam/utils/consts.dart';
import 'package:muslam/utils/pointer.dart';
import 'package:muslam/views/ui/splash/splash_page.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<Prayer> getCurrentPrayer(double lat, double long) async {
  print('>>>>>>>>>>. 1');
  Adhan adhan = AdhanFlutter.create(
    Coordinates(lat, long),
    DateTime.now(),
    CalculationMethod.MUSLIM_WORLD_LEAGUE,
  );

  print('>>>>>>>>>>. 2');

  return await adhan.currentPrayer();
}

// ignore: missing_return
String getPrayerName(Prayer prayer) {
  switch (prayer) {
    case Prayer.NONE:
      return 'غير معروف';
      break;
    case Prayer.FAJR:
      return 'الفجر';
      break;
    case Prayer.SUNRISE:
      return 'الشروق';
      break;
    case Prayer.DHUHR:
      return 'الظهر';
      break;
    case Prayer.ASR:
      return 'العصر';
      break;
    case Prayer.MAGHRIB:
      return 'العصر';
      break;
    case Prayer.ISHA:
      return 'العشاء';
      break;
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  String color = await AppUtils.getSavedColor();
  if (color == null) {
    Pointer.color = maleColor;
  } else if (color == '0') {
    Pointer.color = maleColor;
  } else if (color == '1') {
    Pointer.color = femaleColor;
  } else if (color == '2') {
    Pointer.color = Color(0xff9BBB59);
  } else if (color == '3') {
    Pointer.color = Color(0xffF79646);
  } else {
    Pointer.color = maleColor;
  }

  String gender = await AppUtils.getSavedGender();
  if (gender == null) {
    Pointer.gender = 'male';
  } else if (color == 'male') {
    Pointer.gender = 'male';
  } else {
    Pointer.gender = 'female';
  }

  double fontSize = await AppUtils.getSavedFontSize();
  if (fontSize == null) {
    Pointer.fontSize = 18;
  } else {
    Pointer.fontSize = fontSize;
  }

  Pointer.lat = await AppUtils.getUserLocationLatitude();
  Pointer.long = await AppUtils.getUserLocationLong();

  if (Pointer.lat != null) {
    Prayer p = await getCurrentPrayer(Pointer.lat, Pointer.long);
    Pointer.currentPray = getPrayerName(p);
  }

  Pointer.prayes = await AppUtils.getReadingSalwat();
  Pointer.selectedRak3a = await AppUtils.getSelectedRak3a();

  print('>>Pointer.prayes>>> ${Pointer.prayes}');

  bool isFirstTimeLaunch = await AppUtils.getIsFirstTimeLaunch() ?? true;
  await AppUtils.saveIsFirstTimeLaunch();
  Pointer.isFirstTimeLaunch = isFirstTimeLaunch;

  Pointer.lastPart = await AppUtils.getLastPartData() ?? null;
  print(Pointer.lastPart?.toMap() ?? '');


  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => SettingsProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => PageProvider(),
        ),
        ChangeNotifierProvider(
          create: (_) => NetworkProvider(),
        ),
      ],
      child: MaterialApp(
        title: appName,
        debugShowCheckedModeBanner: false,
        locale: Locale('ar'),
        theme: ThemeData(
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: Directionality(
          child: SplashPage(),
          textDirection: TextDirection.rtl,
        ),
      ),
    );
  }
}
